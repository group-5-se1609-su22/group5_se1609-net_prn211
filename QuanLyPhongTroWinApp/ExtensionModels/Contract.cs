﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyPhongTroWinApp.Models
{
    public partial class Contract
    {
        public string? RoomName
        {
            get { return Room?.Name ?? null; }
        }

        public string? CustomerName
        {
            get { return Customer?.Name ?? null; }
        }

        public decimal? RoomPrice
        {
            get { return Room?.Type?.UnitPrice; }
        }

        public string? RoomType
        {
            get { return Room?.Type?.Name ?? null; }
        }
    }
}
