﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuanLyPhongTroWinApp.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Contracts = new HashSet<Contract>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public DateTime Birthdate { get; set; }
        public string CardId { get; set; }
        public string Role { get; set; }
        public bool Gender { get; set; }
        public string Avatar { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }

        public virtual ICollection<Contract> Contracts { get; set; }
    }
}
