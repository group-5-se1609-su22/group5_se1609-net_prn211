﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuanLyPhongTroWinApp.Models
{
    public partial class Room
    {
        public Room()
        {
            Contracts = new HashSet<Contract>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int TypeId { get; set; }
        public bool Status { get; set; }

        public virtual Type Type { get; set; }
        public virtual ICollection<Contract> Contracts { get; set; }
    }
}
