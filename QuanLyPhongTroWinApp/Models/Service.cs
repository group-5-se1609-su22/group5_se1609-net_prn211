﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuanLyPhongTroWinApp.Models
{
    public partial class Service
    {
        public Service()
        {
            BillServices = new HashSet<BillService>();
        }

        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public decimal UnitPrice { get; set; }
        public bool ServiceStatus { get; set; }

        public virtual ICollection<BillService> BillServices { get; set; }
    }
}
