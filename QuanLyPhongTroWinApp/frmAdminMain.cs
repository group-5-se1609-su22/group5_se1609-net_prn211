﻿using System;
using System.Windows.Forms;
using QuanLyPhongTroWinApp.Models;
namespace QuanLyPhongTroWinApp
{
    public partial class frmAdminMain : Form
    {
        public Customer Customer { get; set; }
        public frmAdminMain()
        {
            InitializeComponent();
        }

        public frmAdminMain(Customer customer)
        {
            InitializeComponent();
            Customer = customer;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
        }

        private void btnCustomerManager_Click(object sender, EventArgs e)
        {
            new frmCustomerManager().ShowDialog();
        }

        private void btnRoomManager_Click(object sender, EventArgs e)
        {
            new frmRoomManager().ShowDialog();
        }

        private void btnTypeManager_Click(object sender, EventArgs e)
        {
            new frmTypeManager().ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge().ShowDialog();
        }

        private void btnServiceManager_Click(object sender, EventArgs e)
        {
            new frmServiceManager().ShowDialog();
        }

        private void btnBillManager_Click(object sender, EventArgs e)
        {
            new frmBillManager().ShowDialog();
        }

        private void btnBillReport_Click(object sender, EventArgs e)
        {
            new frmBillReport().ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            this.Close();
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            new frmChangePassword(Customer).ShowDialog();
        }
    }
}
