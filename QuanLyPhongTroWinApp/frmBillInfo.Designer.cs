﻿namespace QuanLyPhongTroWinApp
{
    partial class frmBillInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbBillInfo = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.btnAdd = new System.Windows.Forms.Button();
            this.rbUnpaid = new System.Windows.Forms.RadioButton();
            this.rbPaid = new System.Windows.Forms.RadioButton();
            this.lbBillStatus = new System.Windows.Forms.Label();
            this.lbStartDate = new System.Windows.Forms.Label();
            this.lbContractID = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lbEndDate = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.dgService = new System.Windows.Forms.DataGridView();
            this.dgBillService = new System.Windows.Forms.DataGridView();
            this.txtUnit = new System.Windows.Forms.TextBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.cbContractID = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgBillService)).BeginInit();
            this.SuspendLayout();
            // 
            // lbBillInfo
            // 
            this.lbBillInfo.AutoSize = true;
            this.lbBillInfo.Font = new System.Drawing.Font("Segoe UI Emoji", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbBillInfo.Location = new System.Drawing.Point(381, 22);
            this.lbBillInfo.Name = "lbBillInfo";
            this.lbBillInfo.Size = new System.Drawing.Size(135, 43);
            this.lbBillInfo.TabIndex = 90;
            this.lbBillInfo.Text = "Bill Info";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtpStartDate.Location = new System.Drawing.Point(576, 82);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(284, 27);
            this.dtpStartDate.TabIndex = 87;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAdd.Location = new System.Drawing.Point(399, 387);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(111, 45);
            this.btnAdd.TabIndex = 86;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // rbUnpaid
            // 
            this.rbUnpaid.AutoSize = true;
            this.rbUnpaid.Checked = true;
            this.rbUnpaid.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbUnpaid.Location = new System.Drawing.Point(190, 2);
            this.rbUnpaid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbUnpaid.Name = "rbUnpaid";
            this.rbUnpaid.Size = new System.Drawing.Size(75, 24);
            this.rbUnpaid.TabIndex = 85;
            this.rbUnpaid.TabStop = true;
            this.rbUnpaid.Text = "Unpaid";
            this.rbUnpaid.UseVisualStyleBackColor = true;
            // 
            // rbPaid
            // 
            this.rbPaid.AutoSize = true;
            this.rbPaid.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbPaid.Location = new System.Drawing.Point(3, 2);
            this.rbPaid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbPaid.Name = "rbPaid";
            this.rbPaid.Size = new System.Drawing.Size(55, 24);
            this.rbPaid.TabIndex = 84;
            this.rbPaid.Text = "Paid";
            this.rbPaid.UseVisualStyleBackColor = true;
            // 
            // lbBillStatus
            // 
            this.lbBillStatus.AutoSize = true;
            this.lbBillStatus.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbBillStatus.Location = new System.Drawing.Point(27, 128);
            this.lbBillStatus.Name = "lbBillStatus";
            this.lbBillStatus.Size = new System.Drawing.Size(77, 21);
            this.lbBillStatus.TabIndex = 75;
            this.lbBillStatus.Text = "Bill Status";
            // 
            // lbStartDate
            // 
            this.lbStartDate.AutoSize = true;
            this.lbStartDate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbStartDate.Location = new System.Drawing.Point(474, 86);
            this.lbStartDate.Name = "lbStartDate";
            this.lbStartDate.Size = new System.Drawing.Size(78, 21);
            this.lbStartDate.TabIndex = 74;
            this.lbStartDate.Text = "Start Date";
            // 
            // lbContractID
            // 
            this.lbContractID.AutoSize = true;
            this.lbContractID.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbContractID.Location = new System.Drawing.Point(27, 86);
            this.lbContractID.Name = "lbContractID";
            this.lbContractID.Size = new System.Drawing.Size(88, 21);
            this.lbContractID.TabIndex = 72;
            this.lbContractID.Text = "Contract ID";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtpEndDate.Location = new System.Drawing.Point(576, 127);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(284, 27);
            this.dtpEndDate.TabIndex = 94;
            // 
            // lbEndDate
            // 
            this.lbEndDate.AutoSize = true;
            this.lbEndDate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbEndDate.Location = new System.Drawing.Point(474, 130);
            this.lbEndDate.Name = "lbEndDate";
            this.lbEndDate.Size = new System.Drawing.Size(72, 21);
            this.lbEndDate.TabIndex = 93;
            this.lbEndDate.Text = "End Date";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbUnpaid);
            this.panel1.Controls.Add(this.rbPaid);
            this.panel1.Location = new System.Drawing.Point(147, 125);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(284, 28);
            this.panel1.TabIndex = 95;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnUpdate.Location = new System.Drawing.Point(399, 387);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(111, 45);
            this.btnUpdate.TabIndex = 96;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // dgService
            // 
            this.dgService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgService.Location = new System.Drawing.Point(27, 176);
            this.dgService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgService.Name = "dgService";
            this.dgService.RowHeadersWidth = 51;
            this.dgService.RowTemplate.Height = 29;
            this.dgService.Size = new System.Drawing.Size(358, 275);
            this.dgService.TabIndex = 98;
            this.dgService.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgService_CellClick);
            // 
            // dgBillService
            // 
            this.dgBillService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBillService.Location = new System.Drawing.Point(516, 176);
            this.dgBillService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgBillService.Name = "dgBillService";
            this.dgBillService.RowHeadersWidth = 51;
            this.dgBillService.RowTemplate.Height = 29;
            this.dgBillService.Size = new System.Drawing.Size(344, 275);
            this.dgBillService.TabIndex = 99;
            this.dgBillService.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBillService_CellClick);
            // 
            // txtUnit
            // 
            this.txtUnit.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtUnit.Location = new System.Drawing.Point(409, 215);
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(83, 27);
            this.txtUnit.TabIndex = 101;
            // 
            // btnConfirm
            // 
            this.btnConfirm.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnConfirm.Location = new System.Drawing.Point(409, 265);
            this.btnConfirm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(83, 40);
            this.btnConfirm.TabIndex = 102;
            this.btnConfirm.Text = ">>";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // cbContractID
            // 
            this.cbContractID.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbContractID.FormattingEnabled = true;
            this.cbContractID.Location = new System.Drawing.Point(147, 82);
            this.cbContractID.Name = "cbContractID";
            this.cbContractID.Size = new System.Drawing.Size(284, 29);
            this.cbContractID.TabIndex = 103;
            // 
            // frmBillInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 477);
            this.Controls.Add(this.cbContractID);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.txtUnit);
            this.Controls.Add(this.dgBillService);
            this.Controls.Add(this.dgService);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.lbEndDate);
            this.Controls.Add(this.lbBillInfo);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbBillStatus);
            this.Controls.Add(this.lbStartDate);
            this.Controls.Add(this.lbContractID);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmBillInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Information";
            this.Load += new System.EventHandler(this.frmBillInfo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgBillService)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbBillInfo;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.RadioButton rbUnpaid;
        private System.Windows.Forms.RadioButton rbPaid;
        private System.Windows.Forms.Label lbBillStatus;
        private System.Windows.Forms.Label lbStartDate;
        private System.Windows.Forms.Label lbContractID;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lbEndDate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGridView dgService;
        private System.Windows.Forms.DataGridView dgBillService;
        private System.Windows.Forms.TextBox txtUnit;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.ComboBox cbContractID;
    }
}