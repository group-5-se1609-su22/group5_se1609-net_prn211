﻿using System;
using System.Windows.Forms;
using QuanLyPhongTroWinApp.Models;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace QuanLyPhongTroWinApp
{
    public partial class frmBillInfo : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private BindingList<Service> sourceService;
        private BindingList<BillService> sourceBillService;
        private bool inserOrUpdate;  // Insert - False; Update - True
        private Bill bill;
        private Service serviceSelected;
        private BillService billServiceSelected;
        private List<BillService> providerBillService;

        public frmBillInfo()
        {
            InitializeComponent();
        }

        public frmBillInfo(bool inserOrUpdate)
        {
            InitializeComponent();
            bill = new Bill();
            this.inserOrUpdate = inserOrUpdate;
        }

        public frmBillInfo(bool inserOrUpdate, Bill bill)
        {
            InitializeComponent();
            this.bill = bill;
            this.inserOrUpdate = inserOrUpdate;
        }

        private void frmBillInfo_Load(object sender, EventArgs e)
        {
            setBtn();
            designDGVService();
            designDGVBillService();
            loadService(context.Services.ToList());
            cbContractID.Items.AddRange(context.Contracts.Select(x => x.Id.ToString().Trim()).ToArray());
            if (inserOrUpdate)
            {
                cbContractID.Text = bill.ContractId.ToString().Trim();
                _ = bill.Status == true ? rbPaid.Checked = true : rbUnpaid.Checked = true;
                dtpStartDate.Value = (DateTime)bill.StartDate;
                dtpEndDate.Value = (DateTime)bill.EndDate;
                providerBillService = context.BillServices.Where(x => x.BillId == bill.Id).ToList();
                loadBillService(providerBillService);
            }

        }


        private void designDGVBillService()
        {
            dgBillService.AutoGenerateColumns = false;
            dgBillService.ReadOnly = true;
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "BillId",
                HeaderText = "BillId",
                Visible = false,
            });
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceName",
                HeaderText = "ServiceName",
            });
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Unit",
                HeaderText = "Unit",
            });
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Price",
                HeaderText = "Price",
            });
        }

        private void designDGVService()
        {
            dgService.AutoGenerateColumns = false;
            dgService.ReadOnly = true;
            dgService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceId",
                HeaderText = "Service ID",
            });
            dgService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceName",
                HeaderText = "Service Name",
            });
            dgService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "UnitPrice",
                HeaderText = "UnitPrice",
            });
            dgService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceStatus",
                HeaderText = "ServiceStatus",
                Visible = false,
            });
        }

        private void loadService(List<Service> services)
        {
            sourceService = new BindingList<Service>(services);
            dgService.DataSource = null;
            dgService.DataSource = sourceService;
        }

        private void loadBillService(List<BillService> billServices)
        {
            sourceBillService = new BindingList<BillService>(billServices);
            dgBillService.DataSource = null;
            dgBillService.DataSource = sourceBillService;
        }
        private void setBtn()
        {
            if (inserOrUpdate)
            {
                this.Text = "Update Product";
                this.btnAdd.Visible = false;
                this.btnUpdate.Visible = true;
            }
            else
            {
                this.Text = "Insert Product";
                this.btnAdd.Visible = true;
                this.btnUpdate.Visible = false;
            }
        }

        private void dgService_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            serviceSelected = dgService.CurrentRow.DataBoundItem as Service;
        }

        private void dgBillService_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            billServiceSelected = dgBillService.CurrentRow.DataBoundItem as BillService;
            txtUnit.Text = billServiceSelected.Unit.ToString();
            serviceSelected = billServiceSelected.Service;
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                int unit = int.Parse(txtUnit.Text.Trim());
                if (unit == 0)
                {
                    BillService bs = providerBillService.SingleOrDefault(x => x.BillId == bill.Id && x.ServiceId == serviceSelected.ServiceId);
                    if (bs != null)
                    {
                        providerBillService.Remove(bs);
                        loadBillService(providerBillService);
                    }
                }
                else
                {
                    BillService billService = new BillService()
                    {
                        BillId = bill.Id,
                        ServiceId = serviceSelected.ServiceId,
                        Unit = unit,
                        Price = serviceSelected.UnitPrice * unit,
                    };
                    providerBillService.Add(billService);
                }

            }
            catch (FormatException)
            {
                MessageBox.Show("Unit must be a number.");
            }
        }

        private bool validateBill()
        {
            bool isValid = true;
            string message = "";
            try
            {
                bill.ContractId = int.Parse(cbContractID.Text.Trim());
            }
            catch (FormatException)
            {
                isValid = false;
                message = "ContractID is requied.\n";
            }
            bill.StartDate = dtpStartDate.Value;
            bill.EndDate = dtpEndDate.Value;
            _ = rbPaid.Checked == true ? bill.Status = true : bill.Status = false;
            if (!isValid)
            {
                MessageBox.Show("[Error] Data Invalid: \n" + message);
            }
            return isValid;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validateBill())
            {
                context.Bills.Add(bill);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    int id = bill.Id;
                    providerBillService.ForEach(x => x.BillId = id);
                    context.BillServices.AddRange(providerBillService);
                    count = context.SaveChanges();
                    if (count > 0)
                    {
                        MessageBox.Show("[Success] Add Bill & BillService Successfullly.", "Notification");
                        bill.TotalPrice = getTotalPrice(bill, providerBillService);
                        context.Bills.Update(bill);
                        context.SaveChanges();
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("[Error] Add Bill Error.", "Error System");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateBill())
            {
                context.Bills.Update(bill);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    context.BillServices.RemoveRange(context.BillServices.Where(x => x.BillId == bill.Id).ToList());
                    context.SaveChanges();
                    context.BillServices.AddRange(providerBillService);
                    count = context.SaveChanges();
                    if (count > 0)
                    {
                        MessageBox.Show("[Success] Update Bill & BillService Successfullly.", "Notification");
                        bill.TotalPrice = getTotalPrice(bill, providerBillService);
                        context.Bills.Update(bill);
                        context.SaveChanges();
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("[Error] Update Bill Error.", "Error System");
                }
            }

        }

        private decimal getTotalPrice(Bill bill, List<BillService> providerBillService)
        {
            Bill _bill = context.Bills.Include(x => x.Contract).ThenInclude(x => x.Room).ThenInclude(x => x.Type).First(x => x.Id == bill.Id);
            decimal totalPriceService = (decimal)providerBillService.Sum(x => x.Price);
            return totalPriceService + _bill.Contract.Room.Type.UnitPrice;
        }
    }
}
