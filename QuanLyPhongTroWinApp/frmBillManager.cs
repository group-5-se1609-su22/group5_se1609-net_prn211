﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Windows.Forms;
using QuanLyPhongTroWinApp.Models;
using System.Linq;
using System.Collections.Generic;

namespace QuanLyPhongTroWinApp
{
    public partial class frmBillManager : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private BindingSource source;

        private Bill billSelected;
        public frmBillManager()
        {
            InitializeComponent();
        }

        private void frmBillManager_Load(object sender, EventArgs e)
        {
            designDGV();
            loadBills(context.Bills.
                Include(bill => bill.Contract)
                    .ThenInclude(contract => contract.Room)
                .Include(bill => bill.Contract)
                    .ThenInclude(contract => contract.Customer)
                .ToList());
            billSelected = dgBillManager.Rows[0].DataBoundItem as Bill;
        }

        private void designDGV()
        {
            dgBillManager.AutoGenerateColumns = false;
            dgBillManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Id",
                HeaderText = "Id",
            });
            dgBillManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ContractId",
                HeaderText = "ContractId",
            });
            dgBillManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomName",
                HeaderText = "Room",
                ReadOnly = true,
            });
            dgBillManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "CustomerName",
                HeaderText = "Customer",
                ReadOnly = true,
            });
            dgBillManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "StartDate",
                HeaderText = "StartDate",
            });
            dgBillManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "EndDate",
                HeaderText = "EndDate",
            });
            dgBillManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "TotalPrice",
                HeaderText = "TotalPrice",
            });
            dgBillManager.Columns.Add(new DataGridViewCheckBoxColumn()
            {
                DataPropertyName = "Status",
                HeaderText = "Status",
            });
        }

        private void loadBills(List<Bill> data)
        {
            source = new BindingSource();
            source.DataSource = data;
            dgBillManager.DataSource = null;
            dgBillManager.DataSource = source;
        }

        private void dgBillManager_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            billSelected = dgBillManager.CurrentRow.DataBoundItem as Bill;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtContractID.Text.Trim().Length != 0)
            {
                try
                {
                    int billID = int.Parse(txtContractID.Text.Trim());
                    var listBillsWithBillID = context.Bills.Where(x => x.Id == billID).ToList();
                    loadBills(listBillsWithBillID);
                    billSelected = dgBillManager.Rows[0].DataBoundItem as Bill;
                }
                catch (FormatException)
                {
                    MessageBox.Show("Bill ID must be a number.");
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Insert - false
            new frmBillInfo(false).ShowDialog();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            new frmBillInfo(true, billSelected).ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            context.Bills.Remove(billSelected);
            int count = context.SaveChanges();
            if (count > 0)
            {
                MessageBox.Show($"[Success] Delete successfully Bill with ID = {billSelected.Id}.", "Notification");
                loadBills(context.Bills.ToList());
            }
            else
            {
                MessageBox.Show($"[Error] Delete error Bill with ID = {billSelected.Id}", "Error System");
            }
        }

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            loadBills(context.Bills.ToList());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCustomerManager_Click(object sender, EventArgs e)
        {
            new frmCustomerManager().ShowDialog();
        }

        private void btnRoomManager_Click(object sender, EventArgs e)
        {
            new frmRoomManager().ShowDialog();
        }

        private void btnTypeManager_Click(object sender, EventArgs e)
        {
            new frmTypeManager().ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge().ShowDialog();
        }

        private void btnServiceManager_Click(object sender, EventArgs e)
        {
            new frmServiceManager().ShowDialog();
        }

        private void btnBillManager_Click(object sender, EventArgs e)
        {
            new frmBillManager().ShowDialog();
        }

        private void btnBillReport_Click(object sender, EventArgs e)
        {
            new frmBillReport().ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }

    }
}
