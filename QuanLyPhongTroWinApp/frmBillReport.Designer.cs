﻿namespace QuanLyPhongTroWinApp
{
    partial class frmBillReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.dgBill = new System.Windows.Forms.DataGridView();
            this.btnExit = new System.Windows.Forms.Button();
            this.dgBillService = new System.Windows.Forms.DataGridView();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lbEndDate = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lbStartDate = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCustomerManager = new System.Windows.Forms.ToolStripMenuItem();
            this.roomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRoomManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTypeManager = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceCharge = new System.Windows.Forms.ToolStripMenuItem();
            this.billStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnBillManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnBillReport = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLogout = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgBillService)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Emoji", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblTitle.Location = new System.Drawing.Point(374, 42);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(177, 43);
            this.lblTitle.TabIndex = 96;
            this.lblTitle.Text = "Bill Report";
            // 
            // dgBill
            // 
            this.dgBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBill.Location = new System.Drawing.Point(25, 191);
            this.dgBill.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgBill.Name = "dgBill";
            this.dgBill.RowHeadersWidth = 51;
            this.dgBill.RowTemplate.Height = 29;
            this.dgBill.Size = new System.Drawing.Size(470, 224);
            this.dgBill.TabIndex = 97;
            this.dgBill.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellClick);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(420, 429);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 37);
            this.btnExit.TabIndex = 98;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // dgBillService
            // 
            this.dgBillService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBillService.Location = new System.Drawing.Point(521, 191);
            this.dgBillService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgBillService.Name = "dgBillService";
            this.dgBillService.RowHeadersWidth = 51;
            this.dgBillService.RowTemplate.Height = 29;
            this.dgBillService.Size = new System.Drawing.Size(342, 224);
            this.dgBillService.TabIndex = 99;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtpEndDate.Location = new System.Drawing.Point(267, 147);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(284, 27);
            this.dtpEndDate.TabIndex = 103;
            // 
            // lbEndDate
            // 
            this.lbEndDate.AutoSize = true;
            this.lbEndDate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbEndDate.Location = new System.Drawing.Point(165, 150);
            this.lbEndDate.Name = "lbEndDate";
            this.lbEndDate.Size = new System.Drawing.Size(72, 21);
            this.lbEndDate.TabIndex = 102;
            this.lbEndDate.Text = "End Date";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtpStartDate.Location = new System.Drawing.Point(267, 102);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(284, 27);
            this.dtpStartDate.TabIndex = 101;
            // 
            // lbStartDate
            // 
            this.lbStartDate.AutoSize = true;
            this.lbStartDate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbStartDate.Location = new System.Drawing.Point(165, 106);
            this.lbStartDate.Name = "lbStartDate";
            this.lbStartDate.Size = new System.Drawing.Size(78, 21);
            this.lbStartDate.TabIndex = 100;
            this.lbStartDate.Text = "Start Date";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Segoe UI Black", 16.125F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.btnSubmit.Location = new System.Drawing.Point(598, 116);
            this.btnSubmit.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(173, 55);
            this.btnSubmit.TabIndex = 104;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.roomToolStripMenuItem,
            this.serviceStripMenuItem,
            this.billStripMenuItem,
            this.btnLogout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(890, 31);
            this.menuStrip1.TabIndex = 105;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCustomerManager});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(105, 29);
            this.customerToolStripMenuItem.Text = "Customer";
            // 
            // btnCustomerManager
            // 
            this.btnCustomerManager.Name = "btnCustomerManager";
            this.btnCustomerManager.Size = new System.Drawing.Size(246, 30);
            this.btnCustomerManager.Text = "Customer Manager";
            this.btnCustomerManager.Click += new System.EventHandler(this.btnCustomerManager_Click);
            // 
            // roomToolStripMenuItem
            // 
            this.roomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRoomManager,
            this.btnTypeManager});
            this.roomToolStripMenuItem.Name = "roomToolStripMenuItem";
            this.roomToolStripMenuItem.Size = new System.Drawing.Size(72, 29);
            this.roomToolStripMenuItem.Text = "Room";
            // 
            // btnRoomManager
            // 
            this.btnRoomManager.Name = "btnRoomManager";
            this.btnRoomManager.Size = new System.Drawing.Size(213, 30);
            this.btnRoomManager.Text = "Room Manager";
            this.btnRoomManager.Click += new System.EventHandler(this.btnRoomManager_Click);
            // 
            // btnTypeManager
            // 
            this.btnTypeManager.Name = "btnTypeManager";
            this.btnTypeManager.Size = new System.Drawing.Size(213, 30);
            this.btnTypeManager.Text = "Type Manager";
            this.btnTypeManager.Click += new System.EventHandler(this.btnTypeManager_Click);
            // 
            // serviceStripMenuItem
            // 
            this.serviceStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnServiceManager,
            this.btnServiceCharge});
            this.serviceStripMenuItem.Name = "serviceStripMenuItem";
            this.serviceStripMenuItem.Size = new System.Drawing.Size(77, 29);
            this.serviceStripMenuItem.Text = "Sevice";
            // 
            // btnServiceManager
            // 
            this.btnServiceManager.Name = "btnServiceManager";
            this.btnServiceManager.Size = new System.Drawing.Size(225, 30);
            this.btnServiceManager.Text = "Service Manager";
            this.btnServiceManager.Click += new System.EventHandler(this.btnServiceManager_Click);
            // 
            // btnServiceCharge
            // 
            this.btnServiceCharge.Name = "btnServiceCharge";
            this.btnServiceCharge.Size = new System.Drawing.Size(225, 30);
            this.btnServiceCharge.Text = "Service Charge";
            this.btnServiceCharge.Click += new System.EventHandler(this.btnServiceCharge_Click);
            // 
            // billStripMenuItem
            // 
            this.billStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBillManager,
            this.btnBillReport});
            this.billStripMenuItem.Name = "billStripMenuItem";
            this.billStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.billStripMenuItem.Text = "Bill";
            // 
            // btnBillManager
            // 
            this.btnBillManager.Name = "btnBillManager";
            this.btnBillManager.Size = new System.Drawing.Size(191, 30);
            this.btnBillManager.Text = "Bill Manager";
            this.btnBillManager.Click += new System.EventHandler(this.btnBillManager_Click);
            // 
            // btnBillReport
            // 
            this.btnBillReport.Name = "btnBillReport";
            this.btnBillReport.Size = new System.Drawing.Size(191, 30);
            this.btnBillReport.Text = "Bill Report";
            this.btnBillReport.Click += new System.EventHandler(this.btnBillReport_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.ForeColor = System.Drawing.Color.Red;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(83, 29);
            this.btnLogout.Text = "Logout";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // frmBillReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 477);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.dtpEndDate);
            this.Controls.Add(this.lbEndDate);
            this.Controls.Add(this.dtpStartDate);
            this.Controls.Add(this.lbStartDate);
            this.Controls.Add(this.dgBillService);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.dgBill);
            this.Controls.Add(this.lblTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmBillReport";
            this.Text = "Bill Statistic";
            this.Load += new System.EventHandler(this.frmBillReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgBillService)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.DataGridView dgBill;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView dgBillService;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Label lbEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Label lbStartDate;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnCustomerManager;
        private System.Windows.Forms.ToolStripMenuItem roomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnRoomManager;
        private System.Windows.Forms.ToolStripMenuItem btnTypeManager;
        private System.Windows.Forms.ToolStripMenuItem serviceStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnServiceManager;
        private System.Windows.Forms.ToolStripMenuItem btnServiceCharge;
        private System.Windows.Forms.ToolStripMenuItem billStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnBillManager;
        private System.Windows.Forms.ToolStripMenuItem btnBillReport;
        private System.Windows.Forms.ToolStripMenuItem btnLogout;
    }
}