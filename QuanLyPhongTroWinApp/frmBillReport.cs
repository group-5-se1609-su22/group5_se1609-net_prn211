﻿using System.Windows.Forms;
using System.Linq;
using QuanLyPhongTroWinApp.Models;
using System;
using System.ComponentModel;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace QuanLyPhongTroWinApp
{
    public partial class frmBillReport : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Bill billSelected;
        BindingList<Bill> billList;
        BindingList<BillService> billListService;

        public frmBillReport()
        {
            InitializeComponent();
        }

        private void frmBillReport_Load(object sender, System.EventArgs e)
        {
            designDGVBill();
            designDGVBillService();

        }

        private void loadBills(List<Bill> bills)
        {
            billList = new BindingList<Bill>(bills);
            dgBill.DataSource = null;
            dgBill.DataSource = billList;
        }

        private void loadBillServices(List<BillService> billServices)
        {
            billListService = new BindingList<BillService>(billServices);
            dgBillService.DataSource = null;
            dgBillService.DataSource = billServices;
        }

        private void designDGVBill()
        {
            dgBill.AutoGenerateColumns = false;
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Id",
                HeaderText = "ID",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomName",
                HeaderText = "Room",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "CustomerName",
                HeaderText = "Customer",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "StartDate",
                HeaderText = "StartDate",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "EndDate",
                HeaderText = "EndDate",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "TotalPrice",
                HeaderText = "TotalPrice",
            });
            dgBill.Columns.Add(new DataGridViewCheckBoxColumn()
            {
                DataPropertyName = "Status",
                HeaderText = "Status",
            });
        }

        private void designDGVBillService()
        {
            dgBillService.AutoGenerateColumns = false;
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceName",
                HeaderText = "ServiceName",
            });
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Unit",
                HeaderText = "Unit",
            });
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Price",
                HeaderText = "Price",
            });
        }

        private void dgBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            billSelected = dgBill.CurrentRow.DataBoundItem as Bill;
            var billservices = context.BillServices.Where(x => x.BillId == billSelected.Id).Include(x => x.Service).ToList();
            loadBillServices(billservices);
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            DateTime startDate = dtpStartDate.Value;
            DateTime endDate = dtpEndDate.Value;
            var bills = context.Bills
                .Where(x => x.StartDate >= startDate && x.EndDate <= endDate)
                .Include(x => x.Contract)
                    .ThenInclude(x => x.Customer)
                .Include(x => x.Contract)
                    .ThenInclude(x => x.Room)
                        .ThenInclude(x => x.Type).ToList();
            loadBills(bills);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCustomerManager_Click(object sender, EventArgs e)
        {
            new frmCustomerManager().ShowDialog();
        }

        private void btnRoomManager_Click(object sender, EventArgs e)
        {
            new frmRoomManager().ShowDialog();
        }

        private void btnTypeManager_Click(object sender, EventArgs e)
        {
            new frmTypeManager().ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge().ShowDialog();
        }

        private void btnServiceManager_Click(object sender, EventArgs e)
        {
            new frmServiceManager().ShowDialog();
        }

        private void btnBillManager_Click(object sender, EventArgs e)
        {
            new frmBillManager().ShowDialog();
        }

        private void btnBillReport_Click(object sender, EventArgs e)
        {
            new frmBillReport().ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }
    }
}
