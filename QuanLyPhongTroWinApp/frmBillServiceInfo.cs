﻿using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmBillServiceInfo : Form
    {
        public bool insertOrUpdate;
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private BillService billService;

        public frmBillServiceInfo()
        {
            InitializeComponent();
        }

        public frmBillServiceInfo(bool insertOrUpdate)
        {
            InitializeComponent();
            this.insertOrUpdate = insertOrUpdate;
            billService = new BillService();
        }

        public frmBillServiceInfo(bool insertOrUpdate, BillService billService)
        {
            InitializeComponent();
            this.insertOrUpdate = insertOrUpdate;
            this.billService = billService;

        }
        private void frmBillServiceInfo_Load(object sender, EventArgs e)
        {
            setBtn();
            cbBillID.Items.AddRange(context.Bills.Select(x => x.Id.ToString().Trim()).ToArray());
            cbServiceID.Items.AddRange(context.Services.Select(x => x.ServiceId.ToString().Trim()).ToArray());
            if (insertOrUpdate)
            {
                cbBillID.Text = billService.BillId.ToString().Trim();
                cbServiceID.Text = billService.ServiceId.ToString().Trim();
                txtUnit.Text = billService.Unit.ToString();
                txtPrice.Text = billService.Price.ToString();
            }
        }

        private void setBtn()
        {
            if (insertOrUpdate)
            {
                this.Text = "Update Bill Service";
                this.btnAdd.Visible = false;
                this.btnUpdate.Visible = true;
                cbBillID.Enabled = false;
                cbServiceID.Enabled = false;
            }
            else
            {
                this.Text = "Insert Bill Service";
                this.btnAdd.Visible = true;
                this.btnUpdate.Visible = false;
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.BillServices.Add(billService);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Add Bill Service Successfully.", "Notification");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("[Error] Add Bill Service Error.", "Error System");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.BillServices.Update(billService);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Update Bill Service Successfully.", "Notification");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("[Error] Update Bill Service Error.", "Error System");
                }
            }
        }

        public bool validateData()
        {
            bool isValid = true;
            string message = "";
            try
            {
                billService.BillId = int.Parse(cbBillID.Text.Trim());
            }
            catch (FormatException)
            {
                isValid = false;
                message = "BillId is requied.\n";
            }
            try
            {
                billService.ServiceId = int.Parse(cbServiceID.Text.Trim());
            }
            catch (FormatException)
            {
                isValid = false;
                message = "ServiceId is requied.\n";
            }
            billService.Service = context.Services.Find(billService.ServiceId);
            try
            {
                billService.Unit = int.Parse(txtUnit.Text.Trim());
                if (billService.Unit <= 0)
                {
                    throw new FormatException();
                }
                billService.Price = billService.Unit * billService.Service.UnitPrice;
            }
            catch (FormatException)
            {
                isValid = false;
                message += "Unit must a real number.\n";
            }
            if (!isValid)
            {
                MessageBox.Show("[Error] Data Invalid: \n" + message, "Validate Error");
            }
            return isValid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
