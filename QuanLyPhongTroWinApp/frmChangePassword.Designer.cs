﻿namespace QuanLyPhongTroWinApp
{
    partial class frmChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNewPassword = new System.Windows.Forms.Label();
            this.lbOldPassword = new System.Windows.Forms.Label();
            this.lbReEnterNewPassword = new System.Windows.Forms.Label();
            this.txtOldPassword = new System.Windows.Forms.TextBox();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.txtReEnterNewPassword = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lbChangePassword = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbNewPassword
            // 
            this.lbNewPassword.AutoSize = true;
            this.lbNewPassword.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbNewPassword.Location = new System.Drawing.Point(68, 291);
            this.lbNewPassword.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbNewPassword.Name = "lbNewPassword";
            this.lbNewPassword.Size = new System.Drawing.Size(356, 50);
            this.lbNewPassword.TabIndex = 1;
            this.lbNewPassword.Text = "Enter New Password";
            // 
            // lbOldPassword
            // 
            this.lbOldPassword.AutoSize = true;
            this.lbOldPassword.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbOldPassword.Location = new System.Drawing.Point(68, 185);
            this.lbOldPassword.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbOldPassword.Name = "lbOldPassword";
            this.lbOldPassword.Size = new System.Drawing.Size(341, 50);
            this.lbOldPassword.TabIndex = 2;
            this.lbOldPassword.Text = "Enter Old Password";
            // 
            // lbReEnterNewPassword
            // 
            this.lbReEnterNewPassword.AutoSize = true;
            this.lbReEnterNewPassword.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbReEnterNewPassword.Location = new System.Drawing.Point(68, 397);
            this.lbReEnterNewPassword.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbReEnterNewPassword.Name = "lbReEnterNewPassword";
            this.lbReEnterNewPassword.Size = new System.Drawing.Size(411, 50);
            this.lbReEnterNewPassword.TabIndex = 3;
            this.lbReEnterNewPassword.Text = "Re-enter New Password";
            // 
            // txtOldPassword
            // 
            this.txtOldPassword.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtOldPassword.Location = new System.Drawing.Point(530, 185);
            this.txtOldPassword.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.txtOldPassword.Name = "txtOldPassword";
            this.txtOldPassword.PasswordChar = '*';
            this.txtOldPassword.Size = new System.Drawing.Size(509, 56);
            this.txtOldPassword.TabIndex = 5;
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtNewPassword.Location = new System.Drawing.Point(530, 286);
            this.txtNewPassword.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '*';
            this.txtNewPassword.Size = new System.Drawing.Size(509, 56);
            this.txtNewPassword.TabIndex = 6;
            // 
            // txtReEnterNewPassword
            // 
            this.txtReEnterNewPassword.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtReEnterNewPassword.Location = new System.Drawing.Point(530, 392);
            this.txtReEnterNewPassword.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.txtReEnterNewPassword.Name = "txtReEnterNewPassword";
            this.txtReEnterNewPassword.PasswordChar = '*';
            this.txtReEnterNewPassword.Size = new System.Drawing.Size(509, 56);
            this.txtReEnterNewPassword.TabIndex = 7;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSubmit.Location = new System.Drawing.Point(381, 495);
            this.btnSubmit.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(310, 77);
            this.btnSubmit.TabIndex = 8;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lbChangePassword
            // 
            this.lbChangePassword.AutoSize = true;
            this.lbChangePassword.Font = new System.Drawing.Font("Segoe UI Emoji", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbChangePassword.Location = new System.Drawing.Point(270, 52);
            this.lbChangePassword.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbChangePassword.Name = "lbChangePassword";
            this.lbChangePassword.Size = new System.Drawing.Size(545, 85);
            this.lbChangePassword.TabIndex = 92;
            this.lbChangePassword.Text = "Change Password";
            // 
            // frmChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1102, 631);
            this.Controls.Add(this.lbChangePassword);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtReEnterNewPassword);
            this.Controls.Add(this.txtNewPassword);
            this.Controls.Add(this.txtOldPassword);
            this.Controls.Add(this.lbReEnterNewPassword);
            this.Controls.Add(this.lbOldPassword);
            this.Controls.Add(this.lbNewPassword);
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "frmChangePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbNewPassword;
        private System.Windows.Forms.Label lbOldPassword;
        private System.Windows.Forms.Label lbReEnterNewPassword;
        private System.Windows.Forms.TextBox txtOldPassword;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.TextBox txtReEnterNewPassword;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lbChangePassword;
    }
}