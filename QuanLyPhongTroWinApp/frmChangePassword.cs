﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyPhongTroWinApp.Models;

namespace QuanLyPhongTroWinApp
{
    public partial class frmChangePassword : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Customer customer;
        public frmChangePassword()
        {
            InitializeComponent();
        }

        public frmChangePassword(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Customers.Update(customer);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Update Password Successfully.", "Notification");
                    this.Close();
                }
            }
        }

        private bool validateData()
        {
            if (txtOldPassword.Text.Trim().Equals(this.customer))
            {
                MessageBox.Show("[Error] Old password is not correct.", "Error Data");
                return false;
            }
            if (txtOldPassword.Text.Trim().Equals(txtReEnterNewPassword.Text.Trim()))
            {
                MessageBox.Show("[Error] New password is not comfirm.", "Error Data");
                return false;
            }
            customer.Password = txtOldPassword.Text.Trim();
            return true;
        }
    }
}
