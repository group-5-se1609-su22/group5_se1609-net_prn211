﻿using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmCustomerInfo : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private bool inserOrUpdate;  // Insert - False; Update - True
        private Customer customer;
        public frmCustomerInfo()
        {
            InitializeComponent();
        }
        public frmCustomerInfo(bool inserOrUpdate)
        {
            InitializeComponent();
            this.inserOrUpdate = inserOrUpdate;
            this.customer = new Customer();
        }

        public frmCustomerInfo(bool inserOrUpdate, Customer customer)
        {
            InitializeComponent();
            this.inserOrUpdate = inserOrUpdate;
            this.customer = customer;
        }
        private async void frmCustomerInfo_Load(object sender, EventArgs e)
        {
            setBtn();
            if (inserOrUpdate)
            {
                txtCustomerName.Text = customer.Name;
                txtPhoneNumber.Text = customer.Phone;
                dtpDatebirth.Value = (DateTime)customer.Birthdate;
                _ = customer.Gender == true ? rbMale.Checked = true : rbFemale.Checked = true;
                txtAddress.Text = customer.Address;
                txtCardID.Text = customer.CardId;
                txtUsername.Text = customer.Username;
                txtPassword.Text = customer.Password;
                pbAvatar.ImageLocation = Application.StartupPath + $@"{customer.Avatar}";
                pbAvatar.SizeMode = PictureBoxSizeMode.Zoom;
            }
        }

        private void setBtn()
        {
            if (inserOrUpdate)
            {
                this.Text = "Update Customer Info";
                this.btnAdd.Visible = false;
                this.btnUpdate.Visible = true;
                this.txtUsername.ReadOnly = true;
                this.txtPassword.ReadOnly = true;
            }
            else
            {
                this.Text = "Insert Customer Info";
                this.btnAdd.Visible = true;
                this.btnUpdate.Visible = false;
            }
        }

        private bool validateData()
        {
            bool isValid = true;
            string message = "";
            customer.Name = txtCustomerName.Text.Trim();
            customer.Phone = txtPhoneNumber.Text.Trim();
            customer.Birthdate = dtpDatebirth.Value;
            customer.Gender = rbMale.Checked == true;
            customer.Address = txtAddress.Text.Trim();
            customer.CardId = txtCardID.Text.Trim();
            customer.Username = txtUsername.Text.Trim();
            customer.Password = txtPassword.Text.Trim();
            if (string.IsNullOrEmpty(customer.Name))
            {
                isValid = false;
                message += "Name is required.\n";
            }
            if (string.IsNullOrEmpty(customer.Phone))
            {
                isValid = false;
                message += "Phone is required.\n";
            }
            if (string.IsNullOrEmpty(customer.Birthdate.ToString()))
            {
                isValid = false;
                message += "Birthdate is required.\n";
            }
            if (string.IsNullOrEmpty(customer.Address))
            {
                isValid = false;
                message += "Address is required.\n";
            }
            if (string.IsNullOrEmpty(customer.CardId))
            {
                isValid = false;
                message += "Card Id is required.\n";
            }
            if (string.IsNullOrEmpty(customer.Username))
            {
                isValid = false;
                message += "Username is required.\n";
            }
            if (string.IsNullOrEmpty(customer.Password))
            {
                isValid = false;
                message += "Password is required.\n";
            }
            if (!isValid)
            {
                MessageBox.Show("[Error] Data Invalid: \n" + message);
            }
            return isValid;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Customers.Add(customer);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show($"[Success] Add Customer Successfully.", "Notification");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("[Error] Add Customer Error.", "Error System");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Customers.Update(customer);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show($"[Success] Update Customer Successfully.", "Notification");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("[Error] Add Customer Error.", "Error System");
                }
            }
        }
    }
}
