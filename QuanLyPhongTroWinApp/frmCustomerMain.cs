﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyPhongTroWinApp.Models;

namespace QuanLyPhongTroWinApp
{
    public partial class frmCustomerMain : Form
    {
        private Customer customer;
        public frmCustomerMain()
        {
            InitializeComponent();
        }

        public frmCustomerMain(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
        }

        private void btnMyProfile_Click(object sender, EventArgs e)
        {
            new frmCustomerProfile(customer).ShowDialog();
        }

        private void btnRentedRoom_Click(object sender, EventArgs e)
        {
            new frmRentedRoom(customer).ShowDialog();
        }
        private void btnPaymentHistory_Click(object sender, EventArgs e)
        {
            new frmPaymentHistory(customer).ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge(customer).ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            this.Close();
        }

    }
}
