﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using QuanLyPhongTroWinApp.Models;
namespace QuanLyPhongTroWinApp
{
    public partial class frmCustomerManager : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Customer customerSeleted;
        private BindingList<Customer> customerList;

        public frmCustomerManager()
        {
            InitializeComponent();
        }

        private void frmCustomerManager_Load(object sender, System.EventArgs e)
        {
            designDGV();
            loadCustomers(context.Customers.ToList());
            customerSeleted = dgCustomer.Rows[0].DataBoundItem as Customer;
        }

        private void designDGV()
        {
            dgCustomer.AutoGenerateColumns = false;
            dgCustomer.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Id",
                HeaderText = "Id",
            });
            dgCustomer.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Name",
                HeaderText = "Name",
            });
            dgCustomer.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Phone",
                HeaderText = "Phone",
            });
            dgCustomer.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Birthdate",
                HeaderText = "Birthdate",
            });
            dgCustomer.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "CardId",
                HeaderText = "CardId",
            });
            dgCustomer.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "GenderString",
                HeaderText = "Gender",
            });
            dgCustomer.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Username",
                HeaderText = "Username",
            });
            dgCustomer.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Address",
                HeaderText = "Address",
            });
        }

        private void loadCustomers(List<Customer> customers)
        {
            customerList = new BindingList<Customer>(customers);
            dgCustomer.DataSource = null;
            dgCustomer.DataSource = customerList;
        }

        private void dgCustomer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            customerSeleted = dgCustomer.CurrentRow.DataBoundItem as Customer;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmCustomerInfo(false).ShowDialog();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            new frmCustomerInfo(true, customerSeleted).ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            context.Customers.Remove(customerSeleted);
            int count = context.SaveChanges();
            if (count > 0)
            {
                MessageBox.Show($"[Success] Delete successfully Customer with ID = {customerSeleted.Id}.", "Notification");
                loadCustomers(context.Customers.ToList());
            }
            else
            {
                MessageBox.Show($"[Error] Delete error Customer with ID = {customerSeleted.Id}.", "Error System");
            }
        }

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            loadCustomers(context.Customers.ToList());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCustomerManager_Click(object sender, EventArgs e)
        {
            new frmCustomerManager().ShowDialog();
        }

        private void btnRoomManager_Click(object sender, EventArgs e)
        {
            new frmRoomManager().ShowDialog();
        }

        private void btnTypeManager_Click(object sender, EventArgs e)
        {
            new frmTypeManager().ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge().ShowDialog();
        }

        private void btnServiceManager_Click(object sender, EventArgs e)
        {
            new frmServiceManager().ShowDialog();
        }

        private void btnBillManager_Click(object sender, EventArgs e)
        {
            new frmBillManager().ShowDialog();
        }

        private void btnBillReport_Click(object sender, EventArgs e)
        {
            new frmBillReport().ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }
    }
}
