﻿using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmCustomerProfile : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Customer customer;
        public frmCustomerProfile()
        {
            InitializeComponent();
        }

        public frmCustomerProfile(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
        }

        private void frmCustomerProfile_Load(object sender, EventArgs e)
        {
            loadProfile();
        }

        private void loadProfile()
        {
            txtName.Text = customer.Name;
            txtPhone.Text = customer.Phone;
            dtpBirth.Value = (DateTime)customer.Birthdate;
            _ = customer.Gender == true ? rbMale.Checked = true : rbFemale.Checked = true;
            txtAddress.Text = customer.Address;
            txtCardID.Text = customer.CardId;
            txtUsername.Text = customer.Username;
            txtPassword.Text = customer.Password;
            pbAvatar.ImageLocation = Application.StartupPath + $@"{customer.Avatar}";
            pbAvatar.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Update(customer);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Update Profile Successfully", "Notification");
                    loadProfile();
                }
                else
                {
                    MessageBox.Show("[Error] Update Profile Error", "Error System");
                }
            }

        }

        private bool validateData()
        {
            bool isValid = true;
            string message = "";
            customer.Name = txtName.Text.Trim();
            customer.Phone = txtPhone.Text.Trim();
            customer.Birthdate = dtpBirth.Value;
            customer.Gender = rbMale.Checked == true;
            customer.Address = txtAddress.Text.Trim();
            customer.CardId = txtCardID.Text.Trim();
            if (string.IsNullOrEmpty(customer.Name))
            {
                isValid = false;
                message += "Name is required.\n";
            }
            if (string.IsNullOrEmpty(customer.Phone))
            {
                isValid = false;
                message += "Phone is required.\n";
            }
            if (string.IsNullOrEmpty(customer.Birthdate.ToString()))
            {
                isValid = false;
                message += "Birthdate is required.\n";
            }
            if (string.IsNullOrEmpty(customer.Address))
            {
                isValid = false;
                message += "Address is required.\n";
            }
            if (string.IsNullOrEmpty(customer.CardId))
            {
                isValid = false;
                message += "Card Id is required.\n";
            }
            if (!isValid)
            {
                MessageBox.Show("[Error] Data Invalid: \n" + message);
            }
            return isValid;
        }

        private void btnMyProfile_Click(object sender, EventArgs e)
        {
            new frmCustomerProfile(customer).ShowDialog();
        }

        private void btnRentedRoom_Click(object sender, EventArgs e)
        {
            new frmRentedRoom(customer).ShowDialog();
        }
        private void btnPaymentHistory_Click(object sender, EventArgs e)
        {
            new frmPaymentHistory(customer).ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge(customer).ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }
    }
}
