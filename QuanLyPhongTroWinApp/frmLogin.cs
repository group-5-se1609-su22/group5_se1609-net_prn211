﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyPhongTroWinApp.Models;

namespace QuanLyPhongTroWinApp
{
    public partial class frmLogin : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var user = context.Customers.SingleOrDefault(item => item.Username == lAcc.Text && item.Password == lPass.Text);
            if (user != null)
            {
                this.Hide();
                if (user.Role.Equals("adminstrator"))
                {
                    new frmAdminMain(user).Show();
                }
                if (user.Role.Equals("customer"))
                {
                    new frmCustomerMain(user).Show();
                }
            }
            else
            {
                MessageBox.Show("[Error] Account/Password Invalid!!!", "Login Failed");
            }
        }

        private void frmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
