﻿namespace QuanLyPhongTroWinApp
{
    partial class frmPaymentHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.lbPaymentHistory = new System.Windows.Forms.Label();
            this.dgBill = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.myAccountManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMyProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.myRoomManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRentedRoom = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPaymentHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceCharge = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.dgBillService = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBillService)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(760, 418);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 37);
            this.btnExit.TabIndex = 95;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lbPaymentHistory
            // 
            this.lbPaymentHistory.AutoSize = true;
            this.lbPaymentHistory.Font = new System.Drawing.Font("Segoe UI Emoji", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbPaymentHistory.Location = new System.Drawing.Point(316, 46);
            this.lbPaymentHistory.Name = "lbPaymentHistory";
            this.lbPaymentHistory.Size = new System.Drawing.Size(267, 43);
            this.lbPaymentHistory.TabIndex = 94;
            this.lbPaymentHistory.Text = "Payment History";
            // 
            // dgBill
            // 
            this.dgBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBill.Location = new System.Drawing.Point(12, 118);
            this.dgBill.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgBill.Name = "dgBill";
            this.dgBill.RowHeadersWidth = 51;
            this.dgBill.RowTemplate.Height = 29;
            this.dgBill.Size = new System.Drawing.Size(491, 296);
            this.dgBill.TabIndex = 93;
            this.dgBill.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBill_CellClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.myAccountManagerToolStripMenuItem,
            this.myRoomManagerToolStripMenuItem,
            this.toolStripMenuItem2,
            this.btnLogout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(890, 31);
            this.menuStrip1.TabIndex = 96;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // myAccountManagerToolStripMenuItem
            // 
            this.myAccountManagerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMyProfile});
            this.myAccountManagerToolStripMenuItem.Name = "myAccountManagerToolStripMenuItem";
            this.myAccountManagerToolStripMenuItem.Size = new System.Drawing.Size(124, 29);
            this.myAccountManagerToolStripMenuItem.Text = "My Account";
            // 
            // btnMyProfile
            // 
            this.btnMyProfile.Name = "btnMyProfile";
            this.btnMyProfile.Size = new System.Drawing.Size(170, 30);
            this.btnMyProfile.Text = "My Profile";
            this.btnMyProfile.Click += new System.EventHandler(this.btnMyProfile_Click);
            // 
            // myRoomManagerToolStripMenuItem
            // 
            this.myRoomManagerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRentedRoom});
            this.myRoomManagerToolStripMenuItem.Name = "myRoomManagerToolStripMenuItem";
            this.myRoomManagerToolStripMenuItem.Size = new System.Drawing.Size(103, 29);
            this.myRoomManagerToolStripMenuItem.Text = "My Room";
            // 
            // btnRentedRoom
            // 
            this.btnRentedRoom.Name = "btnRentedRoom";
            this.btnRentedRoom.Size = new System.Drawing.Size(195, 30);
            this.btnRentedRoom.Text = "Rented Room";
            this.btnRentedRoom.Click += new System.EventHandler(this.btnRentedRoom_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPaymentHistory,
            this.btnServiceCharge});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(50, 29);
            this.toolStripMenuItem2.Text = "Bill";
            // 
            // btnPaymentHistory
            // 
            this.btnPaymentHistory.Name = "btnPaymentHistory";
            this.btnPaymentHistory.Size = new System.Drawing.Size(220, 30);
            this.btnPaymentHistory.Text = "Payment History";
            this.btnPaymentHistory.Click += new System.EventHandler(this.frmPaymentHistory_Load);
            // 
            // btnServiceCharge
            // 
            this.btnServiceCharge.Name = "btnServiceCharge";
            this.btnServiceCharge.Size = new System.Drawing.Size(220, 30);
            this.btnServiceCharge.Text = "Service Charge";
            this.btnServiceCharge.Click += new System.EventHandler(this.btnServiceCharge_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.ForeColor = System.Drawing.Color.Red;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(83, 29);
            this.btnLogout.Text = "Logout";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // dgBillService
            // 
            this.dgBillService.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBillService.Location = new System.Drawing.Point(526, 118);
            this.dgBillService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgBillService.Name = "dgBillService";
            this.dgBillService.RowHeadersWidth = 51;
            this.dgBillService.RowTemplate.Height = 29;
            this.dgBillService.Size = new System.Drawing.Size(352, 296);
            this.dgBillService.TabIndex = 97;
            // 
            // frmPaymentHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 477);
            this.Controls.Add(this.dgBillService);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lbPaymentHistory);
            this.Controls.Add(this.dgBill);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmPaymentHistory";
            this.Text = "Payment History";
            this.Load += new System.EventHandler(this.frmPaymentHistory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgBill)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBillService)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lbPaymentHistory;
        private System.Windows.Forms.DataGridView dgBill;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem myAccountManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnMyProfile;
        private System.Windows.Forms.ToolStripMenuItem myRoomManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnRentedRoom;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem btnPaymentHistory;
        private System.Windows.Forms.ToolStripMenuItem btnServiceCharge;
        private System.Windows.Forms.ToolStripMenuItem btnLogout;
        private System.Windows.Forms.DataGridView dgBillService;
    }
}