﻿using Microsoft.EntityFrameworkCore;
using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmPaymentHistory : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Customer customer;
        private BindingList<Bill> sourceBills;
        private BindingList<BillService> sourceBillServices;
        private Bill billSelected;
        public frmPaymentHistory()
        {
            InitializeComponent();
        }

        public frmPaymentHistory(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
        }

        private void frmPaymentHistory_Load(object sender, EventArgs e)
        {
            designDGVBill();
            designDGVBillService();
            loadBill(context.Bills
                .Include(x => x.BillServices)
                .Include(x => x.Contract)
                    .ThenInclude(x => x.Room)
                        .ThenInclude(x => x.Type)
                .Where(x => x.Contract.Customer.Id == customer.Id)
                .ToList());
        }

        private void designDGVBill()
        {
            dgBill.AutoGenerateColumns = false;
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Id",
                HeaderText = "ID",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomName",
                HeaderText = "Room",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "StartDate",
                HeaderText = "StartDate",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "EndDate",
                HeaderText = "EndDate",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomPrice",
                HeaderText = "RoomPrice",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServicePrice",
                HeaderText = "ServicePrice",
            });
            dgBill.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "TotalPrice",
                HeaderText = "TotalPrice",
            });
            dgBill.Columns.Add(new DataGridViewCheckBoxColumn()
            {
                DataPropertyName = "Status",
                HeaderText = "Status",
            });
        }
        private void designDGVBillService()
        {
            dgBillService.AutoGenerateColumns = false;
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceName",
                HeaderText = "ServiceName",
            });
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Unit",
                HeaderText = "Unit",
            });
            dgBillService.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Price",
                HeaderText = "Price",
            });
        }

        private void loadBill(List<Bill> bills)
        {
            sourceBills = new BindingList<Bill>(bills);
            dgBill.DataSource = null;
            dgBill.DataSource = sourceBills;
        }

        private void loadBillService(List<BillService> billServices)
        {
            sourceBillServices = new BindingList<BillService>(billServices);
            dgBillService.DataSource = null;
            dgBillService.DataSource = sourceBillServices;
        }

        private void dgBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            billSelected = dgBill.CurrentRow.DataBoundItem as Bill;
            loadBillService(context.BillServices.Where(x => x.BillId == billSelected.Id).Include(x => x.Service).ToList());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMyProfile_Click(object sender, EventArgs e)
        {
            new frmCustomerProfile(customer).ShowDialog();
        }

        private void btnRentedRoom_Click(object sender, EventArgs e)
        {
            new frmRentedRoom(customer).ShowDialog();
        }
        private void btnPaymentHistory_Click(object sender, EventArgs e)
        {
            new frmPaymentHistory(customer).ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge(customer).ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }
    }
}
