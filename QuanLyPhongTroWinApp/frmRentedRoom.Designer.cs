﻿namespace QuanLyPhongTroWinApp
{
    partial class frmRentedRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgRentedRoom = new System.Windows.Forms.DataGridView();
            this.lbRentedRoom = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.myAccountManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMyProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.myRoomManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRentedRoom = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPaymentHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceCharge = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLogout = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgRentedRoom)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgRentedRoom
            // 
            this.dgRentedRoom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRentedRoom.Location = new System.Drawing.Point(24, 91);
            this.dgRentedRoom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgRentedRoom.Name = "dgRentedRoom";
            this.dgRentedRoom.RowHeadersWidth = 51;
            this.dgRentedRoom.RowTemplate.Height = 29;
            this.dgRentedRoom.Size = new System.Drawing.Size(840, 325);
            this.dgRentedRoom.TabIndex = 0;
            // 
            // lbRentedRoom
            // 
            this.lbRentedRoom.AutoSize = true;
            this.lbRentedRoom.Font = new System.Drawing.Font("Segoe UI Emoji", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbRentedRoom.Location = new System.Drawing.Point(354, 42);
            this.lbRentedRoom.Name = "lbRentedRoom";
            this.lbRentedRoom.Size = new System.Drawing.Size(202, 38);
            this.lbRentedRoom.TabIndex = 91;
            this.lbRentedRoom.Text = "Rented Room";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(759, 429);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 37);
            this.btnExit.TabIndex = 92;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.myAccountManagerToolStripMenuItem,
            this.myRoomManagerToolStripMenuItem,
            this.toolStripMenuItem2,
            this.btnLogout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(890, 31);
            this.menuStrip1.TabIndex = 93;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // myAccountManagerToolStripMenuItem
            // 
            this.myAccountManagerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMyProfile});
            this.myAccountManagerToolStripMenuItem.Name = "myAccountManagerToolStripMenuItem";
            this.myAccountManagerToolStripMenuItem.Size = new System.Drawing.Size(124, 29);
            this.myAccountManagerToolStripMenuItem.Text = "My Account";
            // 
            // btnMyProfile
            // 
            this.btnMyProfile.Name = "btnMyProfile";
            this.btnMyProfile.Size = new System.Drawing.Size(170, 30);
            this.btnMyProfile.Text = "My Profile";
            this.btnMyProfile.Click += new System.EventHandler(this.btnMyProfile_Click);
            // 
            // myRoomManagerToolStripMenuItem
            // 
            this.myRoomManagerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRentedRoom});
            this.myRoomManagerToolStripMenuItem.Name = "myRoomManagerToolStripMenuItem";
            this.myRoomManagerToolStripMenuItem.Size = new System.Drawing.Size(103, 29);
            this.myRoomManagerToolStripMenuItem.Text = "My Room";
            // 
            // btnRentedRoom
            // 
            this.btnRentedRoom.Name = "btnRentedRoom";
            this.btnRentedRoom.Size = new System.Drawing.Size(195, 30);
            this.btnRentedRoom.Text = "Rented Room";
            this.btnRentedRoom.Click += new System.EventHandler(this.frmRentedRoom_Load);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPaymentHistory,
            this.btnServiceCharge});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(50, 29);
            this.toolStripMenuItem2.Text = "Bill";
            // 
            // btnPaymentHistory
            // 
            this.btnPaymentHistory.Name = "btnPaymentHistory";
            this.btnPaymentHistory.Size = new System.Drawing.Size(220, 30);
            this.btnPaymentHistory.Text = "Payment History";
            this.btnPaymentHistory.Click += new System.EventHandler(this.btnPaymentHistory_Click);
            // 
            // btnServiceCharge
            // 
            this.btnServiceCharge.Name = "btnServiceCharge";
            this.btnServiceCharge.Size = new System.Drawing.Size(220, 30);
            this.btnServiceCharge.Text = "Service Charge";
            this.btnServiceCharge.Click += new System.EventHandler(this.btnServiceCharge_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.ForeColor = System.Drawing.Color.Red;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(83, 29);
            this.btnLogout.Text = "Logout";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // frmRentedRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 477);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lbRentedRoom);
            this.Controls.Add(this.dgRentedRoom);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmRentedRoom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Your Room";
            this.Load += new System.EventHandler(this.frmRentedRoom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgRentedRoom)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgRentedRoom;
        private System.Windows.Forms.Label lbRentedRoom;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem myAccountManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnMyProfile;
        private System.Windows.Forms.ToolStripMenuItem myRoomManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnRentedRoom;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem btnPaymentHistory;
        private System.Windows.Forms.ToolStripMenuItem btnServiceCharge;
        private System.Windows.Forms.ToolStripMenuItem btnLogout;
    }
}