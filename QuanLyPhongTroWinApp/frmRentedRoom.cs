﻿using Microsoft.EntityFrameworkCore;
using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmRentedRoom : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private BindingSource source;
        private Customer customer;
        public frmRentedRoom()
        {
            InitializeComponent();
        }

        public frmRentedRoom(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
        }

        private void frmRentedRoom_Load(object sender, EventArgs e)
        {
            designDGV();
            loadRented(context.Contracts
                .Where(x => x.Customer.Name == customer.Name)
                .Include(x => x.Customer)
                .Include(x => x.Room)
                    .ThenInclude(x => x.Type)
                .ToList());
        }

        private void designDGV()
        {
            dgRentedRoom.AutoGenerateColumns = false;
            dgRentedRoom.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Id",
                HeaderText = "ID",
            });
            dgRentedRoom.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomName",
                HeaderText = "RoomName",
            });
            dgRentedRoom.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomType",
                HeaderText = "Type",
            });
            dgRentedRoom.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomPrice",
                HeaderText = "Price",
            });
            dgRentedRoom.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "StartDate",
                HeaderText = "StartDate",
            });
            dgRentedRoom.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "EndDate",
                HeaderText = "EndDate",
            });
            dgRentedRoom.Columns.Add(new DataGridViewCheckBoxColumn()
            {
                DataPropertyName = "Status",
                HeaderText = "Status",
            });
        }

        private void loadRented(List<Contract> contracts)
        {
            source = new BindingSource();
            source.DataSource = contracts;
            dgRentedRoom.DataSource = source;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMyProfile_Click(object sender, EventArgs e)
        {
            new frmCustomerProfile(customer).ShowDialog();
        }

        private void btnRentedRoom_Click(object sender, EventArgs e)
        {
            new frmRentedRoom(customer).ShowDialog();
        }
        private void btnPaymentHistory_Click(object sender, EventArgs e)
        {
            new frmPaymentHistory(customer).ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge(customer).ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }
    }
}
