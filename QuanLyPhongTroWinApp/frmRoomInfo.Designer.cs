﻿namespace QuanLyPhongTroWinApp
{
    partial class frmRoomInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbRoomName = new System.Windows.Forms.Label();
            this.lbType = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            this.txtRoomName = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lbRoomInfo = new System.Windows.Forms.Label();
            this.rbAvailable = new System.Windows.Forms.RadioButton();
            this.rbUnavailable = new System.Windows.Forms.RadioButton();
            this.cbTypeID = new System.Windows.Forms.ComboBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnCancer = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbRoomName
            // 
            this.lbRoomName.AutoSize = true;
            this.lbRoomName.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbRoomName.Location = new System.Drawing.Point(90, 79);
            this.lbRoomName.Name = "lbRoomName";
            this.lbRoomName.Size = new System.Drawing.Size(140, 31);
            this.lbRoomName.TabIndex = 0;
            this.lbRoomName.Text = "Room Name";
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbType.Location = new System.Drawing.Point(90, 147);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(68, 31);
            this.lbType.TabIndex = 1;
            this.lbType.Text = "Type ";
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbStatus.Location = new System.Drawing.Point(90, 209);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(76, 31);
            this.lbStatus.TabIndex = 3;
            this.lbStatus.Text = "Status";
            // 
            // txtRoomName
            // 
            this.txtRoomName.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtRoomName.Location = new System.Drawing.Point(257, 69);
            this.txtRoomName.Name = "txtRoomName";
            this.txtRoomName.Size = new System.Drawing.Size(310, 38);
            this.txtRoomName.TabIndex = 4;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAdd.Location = new System.Drawing.Point(167, 291);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(112, 45);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lbRoomInfo
            // 
            this.lbRoomInfo.AutoSize = true;
            this.lbRoomInfo.Font = new System.Drawing.Font("Segoe UI Emoji", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbRoomInfo.Location = new System.Drawing.Point(218, 9);
            this.lbRoomInfo.Name = "lbRoomInfo";
            this.lbRoomInfo.Size = new System.Drawing.Size(217, 53);
            this.lbRoomInfo.TabIndex = 71;
            this.lbRoomInfo.Text = "Room Info";
            // 
            // rbAvailable
            // 
            this.rbAvailable.AutoSize = true;
            this.rbAvailable.Checked = true;
            this.rbAvailable.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbAvailable.Location = new System.Drawing.Point(15, 5);
            this.rbAvailable.Name = "rbAvailable";
            this.rbAvailable.Size = new System.Drawing.Size(129, 35);
            this.rbAvailable.TabIndex = 72;
            this.rbAvailable.TabStop = true;
            this.rbAvailable.Text = "Available";
            this.rbAvailable.UseVisualStyleBackColor = true;
            // 
            // rbUnavailable
            // 
            this.rbUnavailable.AutoSize = true;
            this.rbUnavailable.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rbUnavailable.Location = new System.Drawing.Point(160, 4);
            this.rbUnavailable.Name = "rbUnavailable";
            this.rbUnavailable.Size = new System.Drawing.Size(155, 35);
            this.rbUnavailable.TabIndex = 73;
            this.rbUnavailable.Text = "Unavailable";
            this.rbUnavailable.UseVisualStyleBackColor = true;
            // 
            // cbTypeID
            // 
            this.cbTypeID.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.cbTypeID.FormattingEnabled = true;
            this.cbTypeID.Location = new System.Drawing.Point(257, 136);
            this.cbTypeID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbTypeID.Name = "cbTypeID";
            this.cbTypeID.Size = new System.Drawing.Size(310, 40);
            this.cbTypeID.TabIndex = 74;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnUpdate.Location = new System.Drawing.Point(167, 291);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(112, 45);
            this.btnUpdate.TabIndex = 75;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnCancer
            // 
            this.btnCancer.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnCancer.Location = new System.Drawing.Point(338, 291);
            this.btnCancer.Name = "btnCancer";
            this.btnCancer.Size = new System.Drawing.Size(112, 45);
            this.btnCancer.TabIndex = 76;
            this.btnCancer.Text = "Cancel";
            this.btnCancer.UseVisualStyleBackColor = true;
            this.btnCancer.Click += new System.EventHandler(this.btnCancer_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbUnavailable);
            this.panel1.Controls.Add(this.rbAvailable);
            this.panel1.Location = new System.Drawing.Point(257, 200);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(311, 56);
            this.panel1.TabIndex = 77;
            // 
            // frmRoomInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 351);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnCancer);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.cbTypeID);
            this.Controls.Add(this.lbRoomInfo);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtRoomName);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.lbType);
            this.Controls.Add(this.lbRoomName);
            this.Name = "frmRoomInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Room Information";
            this.Load += new System.EventHandler(this.frmRoomInfo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbRoomName;
        private System.Windows.Forms.Label lbType;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.TextBox txtRoomName;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lbRoomInfo;
        private System.Windows.Forms.RadioButton rbAvailable;
        private System.Windows.Forms.RadioButton rbUnavailable;
        private System.Windows.Forms.ComboBox cbTypeID;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnCancer;
        private System.Windows.Forms.Panel panel1;
    }
}