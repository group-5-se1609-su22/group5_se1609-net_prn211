﻿using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmRoomInfo : Form
    {
        public bool insertOrUpdate;
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Room room;

        public frmRoomInfo()
        {
            InitializeComponent();
        }

        public frmRoomInfo(bool insertOrUpdate)
        {
            InitializeComponent();
            this.insertOrUpdate = insertOrUpdate;
            room = new Room();
        }

        public frmRoomInfo(bool insertOrUpdate, Room room)
        {
            InitializeComponent();
            this.insertOrUpdate = insertOrUpdate;
            this.room = room;
        }
        private void frmRoomInfo_Load(object sender, EventArgs e)
        {
            setBtn();
            cbTypeID.Items.AddRange(context.Types.Select(x => x.Id.ToString().Trim()).ToArray());
            if (insertOrUpdate)
            {
                txtRoomName.Text = room.Name.Trim();
                cbTypeID.Text = room.TypeId.ToString().Trim();
                _ = room.Status == true ? rbAvailable.Checked = true : rbUnavailable.Checked = true;
            }
        }

        private void setBtn()
        {
            if (insertOrUpdate)
            {
                this.Text = "Update Room";
                this.btnAdd.Visible = false;
                this.btnUpdate.Visible = true;
            }
            else
            {
                this.Text = "Insert Room";
                this.btnAdd.Visible = true;
                this.btnUpdate.Visible = false;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Rooms.Add(room);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Add Room Successfully.", "Notification");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("[Error] Add Room Error.", "Error System");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Rooms.Update(room);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Update Room Successfully.", "Notification");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("[Error] Update Room Error.", "Error System");
                }
            }
        }

        public bool validateData()
        {
            bool isValid = true;
            string message = "";
            room.Name = txtRoomName.Text.Trim();
            room.Status = rbAvailable.Checked == true;
            try
            {
                room.TypeId = int.Parse(cbTypeID.Text.Trim());
            }
            catch (FormatException)
            {
                isValid = false;
                message = "TypeID is requied.\n";
            }
            if (string.IsNullOrEmpty(room.Name))
            {
                isValid = false;
                message += "Room Name is required.\n";
            }
            if (!isValid)
            {
                MessageBox.Show("[Error] Data Invalid: \n" + message, "Validate Error");
            }
            return isValid;
        }

        private void btnCancer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
