﻿using System.Windows.Forms;
using System;
using System.Linq;
using QuanLyPhongTroWinApp.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace QuanLyPhongTroWinApp
{
    public partial class frmRoomManager : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Room roomSelected;
        BindingSource source;

        public frmRoomManager()
        {
            InitializeComponent();
        }

        private void frmRoomManager_Load(object sender, EventArgs e)
        {
            designDGV();
            loadRooms(context.Rooms.Include(x => x.Type).Include(x => x.Contracts).ThenInclude(x => x.Customer).ToList());
            roomSelected = dgRoomManager.Rows[0].DataBoundItem as Room;
        }

        private void designDGV()
        {
            dgRoomManager.AutoGenerateColumns = false;
            dgRoomManager.ReadOnly = true;
            dgRoomManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Id",
                HeaderText = "Id",
            });
            dgRoomManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Name",
                HeaderText = "Name",
            });
            dgRoomManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "CustomerName",
                HeaderText = "Customer",
            });
            dgRoomManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomType",
                HeaderText = "Type",
            });            
            dgRoomManager.Columns.Add(new DataGridViewCheckBoxColumn()
            {
                DataPropertyName = "Status",
                HeaderText = "Status",
            });
        }
        private void loadRooms(List<Room> rooms)
        {
            source = new BindingSource();
            source.DataSource = rooms;
            dgRoomManager.DataSource = null;
            dgRoomManager.DataSource = source;
        }

        private void dgRoomManager_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            roomSelected = dgRoomManager.CurrentRow.DataBoundItem as Room;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmRoomInfo(false).ShowDialog();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            new frmRoomInfo(true, roomSelected).ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            context.Rooms.Remove(roomSelected);
            int count = context.SaveChanges();
            if (count > 0)
            {
                MessageBox.Show($"[Success] Delete Rooms Successfully", "Notification");
                this.Close();
            }
            else
            {
                MessageBox.Show("[Error] Delete Rooms Error", "Error System");
            }
        }

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            loadRooms(context.Rooms.ToList());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCustomerManager_Click(object sender, EventArgs e)
        {
            new frmCustomerManager().ShowDialog();
        }

        private void btnRoomManager_Click(object sender, EventArgs e)
        {
            new frmRoomManager().ShowDialog();
        }

        private void btnTypeManager_Click(object sender, EventArgs e)
        {
            new frmTypeManager().ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge().ShowDialog();
        }

        private void btnServiceManager_Click(object sender, EventArgs e)
        {
            new frmServiceManager().ShowDialog();
        }

        private void btnBillManager_Click(object sender, EventArgs e)
        {
            new frmBillManager().ShowDialog();
        }

        private void btnBillReport_Click(object sender, EventArgs e)
        {
            new frmBillReport().ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }

    }
}
