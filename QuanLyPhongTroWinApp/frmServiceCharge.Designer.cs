﻿namespace QuanLyPhongTroWinApp
{
    partial class frmServiceCharge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.lbServiceCharge = new System.Windows.Forms.Label();
            this.dgServiceCharge = new System.Windows.Forms.DataGridView();
            this.menuStripCustomer = new System.Windows.Forms.MenuStrip();
            this.myAccountManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMyProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.myRoomManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRentedRoom = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPaymentHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceChargeC = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripAdmin = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCustomerManager = new System.Windows.Forms.ToolStripMenuItem();
            this.roomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRoomManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTypeManager = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceChargeA = new System.Windows.Forms.ToolStripMenuItem();
            this.billStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnBillManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnBillReport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgServiceCharge)).BeginInit();
            this.menuStripCustomer.SuspendLayout();
            this.menuStripAdmin.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(756, 418);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 37);
            this.btnExit.TabIndex = 98;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lbServiceCharge
            // 
            this.lbServiceCharge.AutoSize = true;
            this.lbServiceCharge.Font = new System.Drawing.Font("Segoe UI Emoji", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbServiceCharge.Location = new System.Drawing.Point(325, 46);
            this.lbServiceCharge.Name = "lbServiceCharge";
            this.lbServiceCharge.Size = new System.Drawing.Size(243, 43);
            this.lbServiceCharge.TabIndex = 97;
            this.lbServiceCharge.Text = "Service Charge";
            // 
            // dgServiceCharge
            // 
            this.dgServiceCharge.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgServiceCharge.Location = new System.Drawing.Point(12, 110);
            this.dgServiceCharge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgServiceCharge.Name = "dgServiceCharge";
            this.dgServiceCharge.RowHeadersWidth = 51;
            this.dgServiceCharge.RowTemplate.Height = 29;
            this.dgServiceCharge.Size = new System.Drawing.Size(866, 292);
            this.dgServiceCharge.TabIndex = 96;
            // 
            // menuStripCustomer
            // 
            this.menuStripCustomer.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.menuStripCustomer.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripCustomer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.myAccountManagerToolStripMenuItem,
            this.myRoomManagerToolStripMenuItem,
            this.toolStripMenuItem2,
            this.btnLogout});
            this.menuStripCustomer.Location = new System.Drawing.Point(0, 0);
            this.menuStripCustomer.Name = "menuStripCustomer";
            this.menuStripCustomer.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStripCustomer.Size = new System.Drawing.Size(890, 31);
            this.menuStripCustomer.TabIndex = 99;
            this.menuStripCustomer.Text = "menuStrip1";
            // 
            // myAccountManagerToolStripMenuItem
            // 
            this.myAccountManagerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnMyProfile});
            this.myAccountManagerToolStripMenuItem.Name = "myAccountManagerToolStripMenuItem";
            this.myAccountManagerToolStripMenuItem.Size = new System.Drawing.Size(124, 29);
            this.myAccountManagerToolStripMenuItem.Text = "My Account";
            // 
            // btnMyProfile
            // 
            this.btnMyProfile.Name = "btnMyProfile";
            this.btnMyProfile.Size = new System.Drawing.Size(180, 30);
            this.btnMyProfile.Text = "My Profile";
            this.btnMyProfile.Click += new System.EventHandler(this.btnMyProfile_Click);
            // 
            // myRoomManagerToolStripMenuItem
            // 
            this.myRoomManagerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRentedRoom});
            this.myRoomManagerToolStripMenuItem.Name = "myRoomManagerToolStripMenuItem";
            this.myRoomManagerToolStripMenuItem.Size = new System.Drawing.Size(103, 29);
            this.myRoomManagerToolStripMenuItem.Text = "My Room";
            // 
            // btnRentedRoom
            // 
            this.btnRentedRoom.Name = "btnRentedRoom";
            this.btnRentedRoom.Size = new System.Drawing.Size(195, 30);
            this.btnRentedRoom.Text = "Rented Room";
            this.btnRentedRoom.Click += new System.EventHandler(this.btnRentedRoom_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPaymentHistory,
            this.btnServiceChargeC});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(50, 29);
            this.toolStripMenuItem2.Text = "Bill";
            // 
            // btnPaymentHistory
            // 
            this.btnPaymentHistory.Name = "btnPaymentHistory";
            this.btnPaymentHistory.Size = new System.Drawing.Size(220, 30);
            this.btnPaymentHistory.Text = "Payment History";
            this.btnPaymentHistory.Click += new System.EventHandler(this.btnPaymentHistory_Click);
            // 
            // btnServiceChargeC
            // 
            this.btnServiceChargeC.Name = "btnServiceChargeC";
            this.btnServiceChargeC.Size = new System.Drawing.Size(220, 30);
            this.btnServiceChargeC.Text = "Service Charge";
            this.btnServiceChargeC.Click += new System.EventHandler(this.btnServiceChargeC_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.ForeColor = System.Drawing.Color.Red;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(83, 29);
            this.btnLogout.Text = "Logout";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // menuStripAdmin
            // 
            this.menuStripAdmin.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.menuStripAdmin.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripAdmin.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.roomToolStripMenuItem,
            this.serviceStripMenuItem,
            this.billStripMenuItem,
            this.toolStripMenuItem3});
            this.menuStripAdmin.Location = new System.Drawing.Point(0, 31);
            this.menuStripAdmin.Name = "menuStripAdmin";
            this.menuStripAdmin.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStripAdmin.Size = new System.Drawing.Size(890, 31);
            this.menuStripAdmin.TabIndex = 100;
            this.menuStripAdmin.Text = "menuStrip2";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCustomerManager});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(105, 29);
            this.customerToolStripMenuItem.Text = "Customer";
            // 
            // btnCustomerManager
            // 
            this.btnCustomerManager.Name = "btnCustomerManager";
            this.btnCustomerManager.Size = new System.Drawing.Size(246, 30);
            this.btnCustomerManager.Text = "Customer Manager";
            this.btnCustomerManager.Click += new System.EventHandler(this.btnCustomerManager_Click);
            // 
            // roomToolStripMenuItem
            // 
            this.roomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRoomManager,
            this.btnTypeManager});
            this.roomToolStripMenuItem.Name = "roomToolStripMenuItem";
            this.roomToolStripMenuItem.Size = new System.Drawing.Size(72, 29);
            this.roomToolStripMenuItem.Text = "Room";
            // 
            // btnRoomManager
            // 
            this.btnRoomManager.Name = "btnRoomManager";
            this.btnRoomManager.Size = new System.Drawing.Size(213, 30);
            this.btnRoomManager.Text = "Room Manager";
            this.btnRoomManager.Click += new System.EventHandler(this.btnRoomManager_Click);
            // 
            // btnTypeManager
            // 
            this.btnTypeManager.Name = "btnTypeManager";
            this.btnTypeManager.Size = new System.Drawing.Size(213, 30);
            this.btnTypeManager.Text = "Type Manager";
            this.btnTypeManager.Click += new System.EventHandler(this.btnTypeManager_Click);
            // 
            // serviceStripMenuItem
            // 
            this.serviceStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnServiceManager,
            this.btnServiceChargeA});
            this.serviceStripMenuItem.Name = "serviceStripMenuItem";
            this.serviceStripMenuItem.Size = new System.Drawing.Size(77, 29);
            this.serviceStripMenuItem.Text = "Sevice";
            // 
            // btnServiceManager
            // 
            this.btnServiceManager.Name = "btnServiceManager";
            this.btnServiceManager.Size = new System.Drawing.Size(225, 30);
            this.btnServiceManager.Text = "Service Manager";
            this.btnServiceManager.Click += new System.EventHandler(this.btnServiceManager_Click);
            // 
            // btnServiceChargeA
            // 
            this.btnServiceChargeA.Name = "btnServiceChargeA";
            this.btnServiceChargeA.Size = new System.Drawing.Size(225, 30);
            this.btnServiceChargeA.Text = "Service Charge";
            this.btnServiceChargeA.Click += new System.EventHandler(this.btnServiceChargeA_Click);
            // 
            // billStripMenuItem
            // 
            this.billStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBillManager,
            this.btnBillReport});
            this.billStripMenuItem.Name = "billStripMenuItem";
            this.billStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.billStripMenuItem.Text = "Bill";
            // 
            // btnBillManager
            // 
            this.btnBillManager.Name = "btnBillManager";
            this.btnBillManager.Size = new System.Drawing.Size(191, 30);
            this.btnBillManager.Text = "Bill Manager";
            this.btnBillManager.Click += new System.EventHandler(this.btnBillManager_Click);
            // 
            // btnBillReport
            // 
            this.btnBillReport.Name = "btnBillReport";
            this.btnBillReport.Size = new System.Drawing.Size(191, 30);
            this.btnBillReport.Text = "Bill Report";
            this.btnBillReport.Click += new System.EventHandler(this.btnBillReport_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem3.ForeColor = System.Drawing.Color.Red;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(83, 29);
            this.toolStripMenuItem3.Text = "Logout";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // frmServiceCharge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 477);
            this.Controls.Add(this.menuStripAdmin);
            this.Controls.Add(this.menuStripCustomer);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lbServiceCharge);
            this.Controls.Add(this.dgServiceCharge);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmServiceCharge";
            this.Text = "frmServiceCharge";
            this.Load += new System.EventHandler(this.frmServiceCharge_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgServiceCharge)).EndInit();
            this.menuStripCustomer.ResumeLayout(false);
            this.menuStripCustomer.PerformLayout();
            this.menuStripAdmin.ResumeLayout(false);
            this.menuStripAdmin.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lbServiceCharge;
        private System.Windows.Forms.DataGridView dgServiceCharge;
        private System.Windows.Forms.MenuStrip menuStripCustomer;
        private System.Windows.Forms.ToolStripMenuItem myAccountManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnMyProfile;
        private System.Windows.Forms.ToolStripMenuItem myRoomManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnRentedRoom;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem btnPaymentHistory;
        private System.Windows.Forms.ToolStripMenuItem btnServiceChargeC;
        private System.Windows.Forms.ToolStripMenuItem btnLogout;
        private System.Windows.Forms.MenuStrip menuStripAdmin;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnCustomerManager;
        private System.Windows.Forms.ToolStripMenuItem roomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnRoomManager;
        private System.Windows.Forms.ToolStripMenuItem btnTypeManager;
        private System.Windows.Forms.ToolStripMenuItem serviceStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnServiceManager;
        private System.Windows.Forms.ToolStripMenuItem btnServiceChargeA;
        private System.Windows.Forms.ToolStripMenuItem billStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnBillManager;
        private System.Windows.Forms.ToolStripMenuItem btnBillReport;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
    }
}