﻿using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmServiceCharge : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Customer customer;
        BindingSource source;
        public frmServiceCharge()
        {
            InitializeComponent();
            customer = null;
        }

        public frmServiceCharge(Customer customer)
        {
            InitializeComponent();
            this.customer = customer;
        }

        private void setMenuStrip()
        {
            if (customer == null)
            {
                menuStripCustomer.Visible = false;
                menuStripAdmin.Visible = true;
            }
            else
            {
                menuStripCustomer.Visible = true;
                menuStripAdmin.Visible = false;
            }
        }

        private void frmServiceCharge_Load(object sender, EventArgs e)
        {
            setMenuStrip();
            designDGV();
            loadServiceCharge(context.Services.ToList());
        }

        private void designDGV()
        {
            dgServiceCharge.AutoGenerateColumns = false;
            dgServiceCharge.ReadOnly = true;
            dgServiceCharge.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceId",
                HeaderText = "Id",
            });
            dgServiceCharge.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceName",
                HeaderText = "Name",
            });
            dgServiceCharge.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "UnitPrice",
                HeaderText = "UnitPrice",
            });
            dgServiceCharge.Columns.Add(new DataGridViewCheckBoxColumn()
            {
                DataPropertyName = "ServiceStatus",
                HeaderText = "ServiceStatus",
            });
        }

        private void loadServiceCharge(List<Service> services)
        {
            source = new BindingSource();
            source.DataSource = services;
            dgServiceCharge.DataSource = null;
            dgServiceCharge.DataSource = source;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCustomerManager_Click(object sender, EventArgs e)
        {
            new frmCustomerManager().ShowDialog();
        }

        private void btnRoomManager_Click(object sender, EventArgs e)
        {
            new frmRoomManager().ShowDialog();
        }

        private void btnTypeManager_Click(object sender, EventArgs e)
        {
            new frmTypeManager().ShowDialog();
        }

        private void btnServiceChargeA_Click(object sender, EventArgs e)
        {
            new frmServiceCharge().ShowDialog();
        }

        private void btnServiceManager_Click(object sender, EventArgs e)
        {
            new frmServiceManager().ShowDialog();
        }

        private void btnBillManager_Click(object sender, EventArgs e)
        {
            new frmBillManager().ShowDialog();
        }

        private void btnBillReport_Click(object sender, EventArgs e)
        {
            new frmBillReport().ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }

        private void btnMyProfile_Click(object sender, EventArgs e)
        {
            new frmCustomerProfile(customer).ShowDialog();
        }

        private void btnRentedRoom_Click(object sender, EventArgs e)
        {
            new frmRentedRoom(customer).ShowDialog();
        }
        private void btnPaymentHistory_Click(object sender, EventArgs e)
        {
            new frmPaymentHistory(customer).ShowDialog();
        }

        private void btnServiceChargeC_Click(object sender, EventArgs e)
        {
            new frmServiceCharge(customer).ShowDialog();
        }
    }
}
