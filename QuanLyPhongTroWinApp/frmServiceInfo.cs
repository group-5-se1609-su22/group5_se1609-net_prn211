﻿using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmServiceInfo : Form
    {
        private bool insertOrUpdate;
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Service service;
        decimal value;
        public frmServiceInfo()
        {
            InitializeComponent();
        }

        public frmServiceInfo(bool insertOrUpdate)
        {
            InitializeComponent();
            this.insertOrUpdate = insertOrUpdate;
            service = new Service();            
        }

        public frmServiceInfo(bool insertOrUpdate, Service service)
        {
            InitializeComponent();
            this.insertOrUpdate = insertOrUpdate;
            this.service = service;            
        }

        private void frmServiceInfo_Load(object sender, EventArgs e)
        {
            setBtn();
            if (insertOrUpdate)
            {
                txtServiceName.Text = service.ServiceName;
                txtUnitPrice.Text = service.UnitPrice.ToString();
                _ = service.ServiceStatus == true ? rbActive.Checked = true : rbInActive.Checked = true;
            }
        }

        private void setBtn()
        {
            if (insertOrUpdate)
            {
                this.Text = "Update Service";
                this.btnAdd.Visible = false;
                this.btnUpdate.Visible = true;
            }
            else
            {
                this.Text = "Insert Service";
                this.btnAdd.Visible = true;
                this.btnUpdate.Visible = false;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Services.Add(service);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Add Service Successfully.", "Notification");
                }
                else
                {
                    MessageBox.Show("[Error] Add Service Error.", "Error System");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Services.Update(service);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Update Service Successfully", "Notification");
                }
                else
                {
                    MessageBox.Show("[Error] Update Service Error.", "Error System");
                }
            }
        }

        public bool validateData()
        {
            bool isValid = true;
            string message = "";
            service.ServiceName = txtServiceName.Text.Trim();
            service.ServiceStatus = rbActive.Checked == true;
            if (string.IsNullOrEmpty(service.ServiceName))
            {
                isValid = false;
                message += "Service Name is required.\n";
            }
            try
            {
                service.UnitPrice = decimal.Parse(txtUnitPrice.Text.Trim());
                if (service.UnitPrice < 0)
                {
                    throw new FormatException();
                }
            }
            catch (FormatException)
            {
                isValid = false;
                message += "Unit Price must a real number.\n";
            }
            if (!isValid)
            {
                MessageBox.Show("[Error] Data Invalid: \n" + message, "Validate Error");
            }
            return isValid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
