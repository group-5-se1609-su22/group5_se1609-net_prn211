﻿namespace QuanLyPhongTroWinApp
{
    partial class frmServiceManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgServiceManager = new System.Windows.Forms.DataGridView();
            this.btnRefeshService = new System.Windows.Forms.Button();
            this.btnDeleteService = new System.Windows.Forms.Button();
            this.btnUpdateService = new System.Windows.Forms.Button();
            this.btnAddService = new System.Windows.Forms.Button();
            this.dgBillServiceManager = new System.Windows.Forms.DataGridView();
            this.btnRefeshBillService = new System.Windows.Forms.Button();
            this.btnDeleteBillService = new System.Windows.Forms.Button();
            this.btnUpdateBillService = new System.Windows.Forms.Button();
            this.btnAddBillService = new System.Windows.Forms.Button();
            this.lbService = new System.Windows.Forms.Label();
            this.lbBillService = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCustomerManager = new System.Windows.Forms.ToolStripMenuItem();
            this.roomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRoomManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTypeManager = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnServiceCharge = new System.Windows.Forms.ToolStripMenuItem();
            this.billStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnBillManager = new System.Windows.Forms.ToolStripMenuItem();
            this.btnBillReport = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLogout = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgServiceManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgBillServiceManager)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgServiceManager
            // 
            this.dgServiceManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgServiceManager.Location = new System.Drawing.Point(16, 89);
            this.dgServiceManager.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgServiceManager.Name = "dgServiceManager";
            this.dgServiceManager.RowHeadersWidth = 51;
            this.dgServiceManager.RowTemplate.Height = 29;
            this.dgServiceManager.Size = new System.Drawing.Size(400, 325);
            this.dgServiceManager.TabIndex = 35;
            this.dgServiceManager.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgServiceManager_CellClick);
            // 
            // btnRefeshService
            // 
            this.btnRefeshService.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnRefeshService.Location = new System.Drawing.Point(328, 429);
            this.btnRefeshService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRefeshService.Name = "btnRefeshService";
            this.btnRefeshService.Size = new System.Drawing.Size(88, 37);
            this.btnRefeshService.TabIndex = 34;
            this.btnRefeshService.Text = "Refesh";
            this.btnRefeshService.UseVisualStyleBackColor = true;
            this.btnRefeshService.Click += new System.EventHandler(this.btnRefeshService_Click);
            // 
            // btnDeleteService
            // 
            this.btnDeleteService.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnDeleteService.Location = new System.Drawing.Point(225, 429);
            this.btnDeleteService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDeleteService.Name = "btnDeleteService";
            this.btnDeleteService.Size = new System.Drawing.Size(88, 37);
            this.btnDeleteService.TabIndex = 33;
            this.btnDeleteService.Text = "Delete";
            this.btnDeleteService.UseVisualStyleBackColor = true;
            this.btnDeleteService.Click += new System.EventHandler(this.btnDeleteService_Click);
            // 
            // btnUpdateService
            // 
            this.btnUpdateService.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnUpdateService.Location = new System.Drawing.Point(121, 429);
            this.btnUpdateService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateService.Name = "btnUpdateService";
            this.btnUpdateService.Size = new System.Drawing.Size(88, 37);
            this.btnUpdateService.TabIndex = 32;
            this.btnUpdateService.Text = "Update";
            this.btnUpdateService.UseVisualStyleBackColor = true;
            this.btnUpdateService.Click += new System.EventHandler(this.btnUpdateService_Click);
            // 
            // btnAddService
            // 
            this.btnAddService.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAddService.Location = new System.Drawing.Point(16, 429);
            this.btnAddService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddService.Name = "btnAddService";
            this.btnAddService.Size = new System.Drawing.Size(88, 37);
            this.btnAddService.TabIndex = 31;
            this.btnAddService.Text = "Add";
            this.btnAddService.UseVisualStyleBackColor = true;
            this.btnAddService.Click += new System.EventHandler(this.btnAddService_Click);
            // 
            // dgBillServiceManager
            // 
            this.dgBillServiceManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBillServiceManager.Location = new System.Drawing.Point(440, 89);
            this.dgBillServiceManager.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgBillServiceManager.Name = "dgBillServiceManager";
            this.dgBillServiceManager.RowHeadersWidth = 51;
            this.dgBillServiceManager.RowTemplate.Height = 29;
            this.dgBillServiceManager.Size = new System.Drawing.Size(438, 325);
            this.dgBillServiceManager.TabIndex = 37;
            this.dgBillServiceManager.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBillServiceManager_CellClick);
            // 
            // btnRefeshBillService
            // 
            this.btnRefeshBillService.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnRefeshBillService.Location = new System.Drawing.Point(790, 429);
            this.btnRefeshBillService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRefeshBillService.Name = "btnRefeshBillService";
            this.btnRefeshBillService.Size = new System.Drawing.Size(88, 37);
            this.btnRefeshBillService.TabIndex = 41;
            this.btnRefeshBillService.Text = "Refesh";
            this.btnRefeshBillService.UseVisualStyleBackColor = true;
            this.btnRefeshBillService.Click += new System.EventHandler(this.btnRefeshBillService_Click);
            // 
            // btnDeleteBillService
            // 
            this.btnDeleteBillService.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnDeleteBillService.Location = new System.Drawing.Point(670, 429);
            this.btnDeleteBillService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDeleteBillService.Name = "btnDeleteBillService";
            this.btnDeleteBillService.Size = new System.Drawing.Size(88, 37);
            this.btnDeleteBillService.TabIndex = 40;
            this.btnDeleteBillService.Text = "Delete";
            this.btnDeleteBillService.UseVisualStyleBackColor = true;
            this.btnDeleteBillService.Click += new System.EventHandler(this.btnDeleteBillService_Click);
            // 
            // btnUpdateBillService
            // 
            this.btnUpdateBillService.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnUpdateBillService.Location = new System.Drawing.Point(553, 429);
            this.btnUpdateBillService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateBillService.Name = "btnUpdateBillService";
            this.btnUpdateBillService.Size = new System.Drawing.Size(88, 37);
            this.btnUpdateBillService.TabIndex = 39;
            this.btnUpdateBillService.Text = "Update";
            this.btnUpdateBillService.UseVisualStyleBackColor = true;
            this.btnUpdateBillService.Click += new System.EventHandler(this.btnUpdateBillService_Click);
            // 
            // btnAddBillService
            // 
            this.btnAddBillService.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAddBillService.Location = new System.Drawing.Point(440, 429);
            this.btnAddBillService.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddBillService.Name = "btnAddBillService";
            this.btnAddBillService.Size = new System.Drawing.Size(88, 37);
            this.btnAddBillService.TabIndex = 38;
            this.btnAddBillService.Text = "Add";
            this.btnAddBillService.UseVisualStyleBackColor = true;
            this.btnAddBillService.Click += new System.EventHandler(this.btnAddBillService_Click);
            // 
            // lbService
            // 
            this.lbService.AutoSize = true;
            this.lbService.Font = new System.Drawing.Font("Segoe UI Semibold", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lbService.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbService.Location = new System.Drawing.Point(121, 41);
            this.lbService.Name = "lbService";
            this.lbService.Size = new System.Drawing.Size(103, 37);
            this.lbService.TabIndex = 42;
            this.lbService.Text = "Service";
            // 
            // lbBillService
            // 
            this.lbBillService.AutoSize = true;
            this.lbBillService.Font = new System.Drawing.Font("Segoe UI Semibold", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lbBillService.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbBillService.Location = new System.Drawing.Point(599, 41);
            this.lbBillService.Name = "lbBillService";
            this.lbBillService.Size = new System.Drawing.Size(150, 37);
            this.lbBillService.TabIndex = 43;
            this.lbBillService.Text = "Bill Service";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.roomToolStripMenuItem,
            this.serviceStripMenuItem,
            this.billStripMenuItem,
            this.btnLogout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(890, 31);
            this.menuStrip1.TabIndex = 44;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCustomerManager});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(105, 29);
            this.customerToolStripMenuItem.Text = "Customer";
            // 
            // btnCustomerManager
            // 
            this.btnCustomerManager.Name = "btnCustomerManager";
            this.btnCustomerManager.Size = new System.Drawing.Size(246, 30);
            this.btnCustomerManager.Text = "Customer Manager";
            this.btnCustomerManager.Click += new System.EventHandler(this.btnCustomerManager_Click);
            // 
            // roomToolStripMenuItem
            // 
            this.roomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnRoomManager,
            this.btnTypeManager});
            this.roomToolStripMenuItem.Name = "roomToolStripMenuItem";
            this.roomToolStripMenuItem.Size = new System.Drawing.Size(72, 29);
            this.roomToolStripMenuItem.Text = "Room";
            // 
            // btnRoomManager
            // 
            this.btnRoomManager.Name = "btnRoomManager";
            this.btnRoomManager.Size = new System.Drawing.Size(213, 30);
            this.btnRoomManager.Text = "Room Manager";
            this.btnRoomManager.Click += new System.EventHandler(this.btnRoomManager_Click);
            // 
            // btnTypeManager
            // 
            this.btnTypeManager.Name = "btnTypeManager";
            this.btnTypeManager.Size = new System.Drawing.Size(213, 30);
            this.btnTypeManager.Text = "Type Manager";
            this.btnTypeManager.Click += new System.EventHandler(this.btnTypeManager_Click);
            // 
            // serviceStripMenuItem
            // 
            this.serviceStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnServiceManager,
            this.btnServiceCharge});
            this.serviceStripMenuItem.Name = "serviceStripMenuItem";
            this.serviceStripMenuItem.Size = new System.Drawing.Size(77, 29);
            this.serviceStripMenuItem.Text = "Sevice";
            // 
            // btnServiceManager
            // 
            this.btnServiceManager.Name = "btnServiceManager";
            this.btnServiceManager.Size = new System.Drawing.Size(225, 30);
            this.btnServiceManager.Text = "Service Manager";
            this.btnServiceManager.Click += new System.EventHandler(this.btnServiceManager_Click);
            // 
            // btnServiceCharge
            // 
            this.btnServiceCharge.Name = "btnServiceCharge";
            this.btnServiceCharge.Size = new System.Drawing.Size(225, 30);
            this.btnServiceCharge.Text = "Service Charge";
            this.btnServiceCharge.Click += new System.EventHandler(this.btnServiceCharge_Click);
            // 
            // billStripMenuItem
            // 
            this.billStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBillManager,
            this.btnBillReport});
            this.billStripMenuItem.Name = "billStripMenuItem";
            this.billStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.billStripMenuItem.Text = "Bill";
            // 
            // btnBillManager
            // 
            this.btnBillManager.Name = "btnBillManager";
            this.btnBillManager.Size = new System.Drawing.Size(191, 30);
            this.btnBillManager.Text = "Bill Manager";
            this.btnBillManager.Click += new System.EventHandler(this.btnBillManager_Click);
            // 
            // btnBillReport
            // 
            this.btnBillReport.Name = "btnBillReport";
            this.btnBillReport.Size = new System.Drawing.Size(191, 30);
            this.btnBillReport.Text = "Bill Report";
            this.btnBillReport.Click += new System.EventHandler(this.btnBillReport_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.ForeColor = System.Drawing.Color.Red;
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(83, 29);
            this.btnLogout.Text = "Logout";
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // frmServiceManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(890, 477);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.lbBillService);
            this.Controls.Add(this.lbService);
            this.Controls.Add(this.btnRefeshBillService);
            this.Controls.Add(this.btnDeleteBillService);
            this.Controls.Add(this.btnUpdateBillService);
            this.Controls.Add(this.btnAddBillService);
            this.Controls.Add(this.dgBillServiceManager);
            this.Controls.Add(this.dgServiceManager);
            this.Controls.Add(this.btnRefeshService);
            this.Controls.Add(this.btnDeleteService);
            this.Controls.Add(this.btnUpdateService);
            this.Controls.Add(this.btnAddService);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmServiceManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Service Manager";
            this.Load += new System.EventHandler(this.frmServiceManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgServiceManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgBillServiceManager)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgServiceManager;
        private System.Windows.Forms.Button btnRefeshService;
        private System.Windows.Forms.Button btnDeleteService;
        private System.Windows.Forms.Button btnUpdateService;
        private System.Windows.Forms.Button btnAddService;
        private System.Windows.Forms.DataGridView dgBillServiceManager;
        private System.Windows.Forms.Button btnRefeshBillService;
        private System.Windows.Forms.Button btnDeleteBillService;
        private System.Windows.Forms.Button btnUpdateBillService;
        private System.Windows.Forms.Button btnAddBillService;
        private System.Windows.Forms.Label lbService;
        private System.Windows.Forms.Label lbBillService;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnCustomerManager;
        private System.Windows.Forms.ToolStripMenuItem roomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnRoomManager;
        private System.Windows.Forms.ToolStripMenuItem btnTypeManager;
        private System.Windows.Forms.ToolStripMenuItem serviceStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnServiceManager;
        private System.Windows.Forms.ToolStripMenuItem btnServiceCharge;
        private System.Windows.Forms.ToolStripMenuItem billStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnBillManager;
        private System.Windows.Forms.ToolStripMenuItem btnBillReport;
        private System.Windows.Forms.ToolStripMenuItem btnLogout;
    }
}