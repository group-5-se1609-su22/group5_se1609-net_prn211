﻿using Microsoft.EntityFrameworkCore;
using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmServiceManager : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        BindingList<Service> servicesSource;
        BindingList<BillService> billServicesSource;
        private Service serviceSelected;
        private BillService billServiceSelected;

        public frmServiceManager()
        {
            InitializeComponent();
        }

        private void frmServiceManager_Load(object sender, EventArgs e)
        {
            designDGVService();
            designDGVBillService();
            loadServices(context.Services.ToList());
            serviceSelected = dgServiceManager.Rows[0].DataBoundItem as Service;
        }

        private void designDGVBillService()
        {
            dgBillServiceManager.AutoGenerateColumns = false;
            dgBillServiceManager.ReadOnly = true;
            dgBillServiceManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "BillId",
                HeaderText = "BillId",
            });
            dgBillServiceManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "RoomName",
                HeaderText = "Room",
            });
            dgBillServiceManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Unit",
                HeaderText = "Unit",
            });
            dgBillServiceManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Price",
                HeaderText = "Price",
            });
        }

        private void designDGVService()
        {
            dgServiceManager.AutoGenerateColumns = false;
            dgServiceManager.ReadOnly = true;
            dgServiceManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceId",
                HeaderText = "Id",
            });
            dgServiceManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "ServiceName",
                HeaderText = "Name",
            });
            dgServiceManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "UnitPrice",
                HeaderText = "UnitPrice",
            });
            dgServiceManager.Columns.Add(new DataGridViewCheckBoxColumn()
            {
                DataPropertyName = "ServiceStatus",
                HeaderText = "ServiceStatus",
            });
        }

        private void loadBillServices(List<BillService> billServices)
        {
            billServicesSource = new BindingList<BillService>(billServices);
            dgBillServiceManager.DataSource = null;
            dgBillServiceManager.DataSource = billServicesSource;
        }

        private void loadServices(List<Service> services)
        {
            servicesSource = new BindingList<Service>(services);
            dgServiceManager.DataSource = null;
            dgServiceManager.DataSource = servicesSource;
        }

        private void dgServiceManager_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            serviceSelected = dgServiceManager.CurrentRow.DataBoundItem as Service;
            var billServices = context.BillServices.Where(x => x.ServiceId == serviceSelected.ServiceId).Include(x => x.Bill).ThenInclude(x => x.Contract).ThenInclude(x => x.Room).ToList();
            loadBillServices(billServices);
            billServiceSelected = dgBillServiceManager.Rows[0].DataBoundItem as BillService;
        }

        private void btnAddService_Click(object sender, EventArgs e)
        {
            new frmServiceInfo(false).ShowDialog();
            loadServices(context.Services.ToList());
        }

        private void btnUpdateService_Click(object sender, EventArgs e)
        {
            new frmServiceInfo(true, serviceSelected).ShowDialog();
            loadServices(context.Services.ToList());
        }

        private void btnDeleteService_Click(object sender, EventArgs e)
        {
            context.Services.Remove(serviceSelected);
            int count = context.SaveChanges();
            if (count > 0)
            {
                MessageBox.Show("[Success] Delete Service Successfully.", "Notification");
                loadServices(context.Services.ToList());
            }
            else
            {
                MessageBox.Show("[Error] Delete Service Error.", "Error System");
            }
        }

        private void btnRefeshService_Click(object sender, EventArgs e)
        {
            loadServices(context.Services.ToList());
        }

        private void dgBillServiceManager_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            billServiceSelected = dgBillServiceManager.CurrentRow.DataBoundItem as BillService;
        }

        private void btnAddBillService_Click(object sender, EventArgs e)
        {
            new frmBillServiceInfo(false).ShowDialog();
            var billServices = context.BillServices.Where(x => x.ServiceId == serviceSelected.ServiceId).ToList();
            loadBillServices(billServices);
        }

        private void btnUpdateBillService_Click(object sender, EventArgs e)
        {
            new frmBillServiceInfo(true, billServiceSelected).ShowDialog();
            var billServices = context.BillServices.Where(x => x.ServiceId == serviceSelected.ServiceId).ToList();
            loadBillServices(billServices);
        }

        private void btnDeleteBillService_Click(object sender, EventArgs e)
        {
            context.BillServices.Remove(billServiceSelected);
            int count = context.SaveChanges();
            if (count > 0)
            {
                MessageBox.Show("[Success] Delete Bill Service Successfully.", "Notification");
                var billServices = context.BillServices.Where(x => x.ServiceId == serviceSelected.ServiceId).ToList();
                loadBillServices(billServices);
            }
            else
            {
                MessageBox.Show("[Error] Delete Bill Service Error.", "Error System");
            }
        }

        private void btnRefeshBillService_Click(object sender, EventArgs e)
        {
            var billServices = context.BillServices.Where(x => x.ServiceId == serviceSelected.ServiceId).ToList();           
            loadBillServices(billServices);
        }

        private void btnCustomerManager_Click(object sender, EventArgs e)
        {
            new frmCustomerManager().ShowDialog();
        }

        private void btnRoomManager_Click(object sender, EventArgs e)
        {
            new frmRoomManager().ShowDialog();
        }

        private void btnTypeManager_Click(object sender, EventArgs e)
        {
            new frmTypeManager().ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge().ShowDialog();
        }

        private void btnServiceManager_Click(object sender, EventArgs e)
        {
            new frmServiceManager().ShowDialog();
        }

        private void btnBillManager_Click(object sender, EventArgs e)
        {
            new frmBillManager().ShowDialog();
        }

        private void btnBillReport_Click(object sender, EventArgs e)
        {
            new frmBillReport().ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }

    }
}
