﻿using QuanLyPhongTroWinApp.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmTypeInfo : Form
    {
        public bool insertOrUpdate;
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private Models.Type type;
        public frmTypeInfo()
        {
            InitializeComponent();
        }
        public frmTypeInfo(bool insertOrUpdate)
        {
            InitializeComponent();
            this.insertOrUpdate = insertOrUpdate;
            type = new Models.Type();
        }

        public frmTypeInfo(bool insertOrUpdate, Models.Type type)
        {
            InitializeComponent();
            this.insertOrUpdate = insertOrUpdate;
            this.type = type;
        }


        private void frmTypeInfo_Load(object sender, EventArgs e)
        {
            setBtn();
            if (insertOrUpdate)
            {
                txtTypeName.Text = type.Name;
                txtUnitPrice.Text = type.UnitPrice.ToString();
            }
        }

        private void setBtn()
        {
            if (insertOrUpdate)
            {
                this.Text = "Update Type";
                this.btnAdd.Visible = false;
                this.btnUpdate.Visible = true;
            }
            else
            {
                this.Text = "Insert Type";
                this.btnAdd.Visible = true;
                this.btnUpdate.Visible = false;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Types.Add(type);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Add Type Successfully.", "Notification");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("[Error] Add Type Error.", "Error System");
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (validateData())
            {
                context.Types.Update(type);
                int count = context.SaveChanges();
                if (count > 0)
                {
                    MessageBox.Show("[Success] Update Type Successfully.", "Notification");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("[Error] Update Type Error.", "Error System");
                }
            }
        }

        public bool validateData()
        {
            bool isValid = true;
            string message = "";
            type.Name = txtTypeName.Text.Trim();
            if (string.IsNullOrEmpty(type.Name))
            {
                isValid = false;
                message += "Type Name is required.\n";
            }
            try
            {
                type.UnitPrice = decimal.Parse(txtUnitPrice.Text.Trim());
                if (type.UnitPrice < 0)
                {
                    throw new FormatException();
                }
            }
            catch (FormatException)
            {
                isValid = false;
                message += "Unit Price must a real number.\n";
            }
            if (!isValid)
            {
                MessageBox.Show("[Error] Data Invalid: \n" + message, "Validate Error");
            }
            return isValid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
