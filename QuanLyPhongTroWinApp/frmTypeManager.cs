﻿using QuanLyPhongTroWinApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace QuanLyPhongTroWinApp
{
    public partial class frmTypeManager : Form
    {
        PRN211_ProjectContext context = new PRN211_ProjectContext();
        private BindingSource source;
        private Models.Type typeSelected;
        public frmTypeManager()
        {
            InitializeComponent();
        }

        private void frmTypeManager_Load(object sender, EventArgs e)
        {
            designDGV();
            loadType(context.Types.ToList());
            typeSelected = dgTypeManager.Rows[0].DataBoundItem as Models.Type;
        }

        private void designDGV()
        {
            dgTypeManager.AutoGenerateColumns = false;
            dgTypeManager.ReadOnly = true;
            dgTypeManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Id",
                HeaderText = "Id",
            });
            dgTypeManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "Name",
                HeaderText = "Name",
            });
            dgTypeManager.Columns.Add(new DataGridViewTextBoxColumn()
            {
                DataPropertyName = "UnitPrice",
                HeaderText = "Name",
            });
        }

        private void loadType(List<Models.Type> types)
        {
            source = new BindingSource();
            source.DataSource = types;
            dgTypeManager.DataSource = null;
            dgTypeManager.DataSource = source;
        }

        private void dgTypeManager_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            typeSelected = dgTypeManager.CurrentRow.DataBoundItem as Models.Type;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new frmTypeInfo(false).ShowDialog();
            loadType(context.Types.ToList());
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            new frmTypeInfo(true, typeSelected).ShowDialog();
            loadType(context.Types.ToList());
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            context.Types.Remove(typeSelected);
            int count = context.SaveChanges();
            if (count > 0)
            {
                MessageBox.Show($"[Success] Delete Type Successfully.", "Notification");
                loadType(context.Types.ToList());
            }
            else
            {
                MessageBox.Show("[Error] Delete Type Error.", "Error System");
            }
        }

        private void btnRefesh_Click(object sender, EventArgs e)
        {
            loadType(context.Types.ToList());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCustomerManager_Click(object sender, EventArgs e)
        {
            new frmCustomerManager().ShowDialog();
        }

        private void btnRoomManager_Click(object sender, EventArgs e)
        {
            new frmRoomManager().ShowDialog();
        }

        private void btnTypeManager_Click(object sender, EventArgs e)
        {
            new frmTypeManager().ShowDialog();
        }

        private void btnServiceCharge_Click(object sender, EventArgs e)
        {
            new frmServiceCharge().ShowDialog();
        }

        private void btnServiceManager_Click(object sender, EventArgs e)
        {
            new frmServiceManager().ShowDialog();
        }

        private void btnBillManager_Click(object sender, EventArgs e)
        {
            new frmBillManager().ShowDialog();
        }

        private void btnBillReport_Click(object sender, EventArgs e)
        {
            new frmBillReport().ShowDialog();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            new frmLogin().Show();
            for (int i = 1; i < Application.OpenForms.Count; ++i)
                Application.OpenForms[i].Close();
        }
    }
}
