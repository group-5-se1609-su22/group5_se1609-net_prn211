# Rental Property Management for PRN211
Authored and Manage by Group 5 Class 1609-NET SU22

## Description
Our system are very straightforward to use with a simple interface but still provide landlords with fully feature to manage their property. It comes with service management, utility billing, residents management,...

## Built With
- C#
- LinQ
- Entity Framework

## Contributors
**Tran Hai Long (Team Leader)**
- [Profile](https://gitlab.com/longth1407 "Hai Long")
- [Email](mailto:longth1407@gmail.com?subject=Hi% "Hi!")

**Nguyen Van Long**
- [Profile](https://gitlab.com/longnvhe151244 "Van Long")
- [Email](mailto:longnvhe151244@fpt.edu.vn?subject=Hi% "Hi!")

**Nguyen Tien Dat**
- [Profile](https://gitlab.com/NguyenTien-Dat "Tien Dat")
- [Email](mailto:datnthe151418@fpt.edu.vn?subject=Hi% "Hi!")

## Acknowledgment
**Ta Dinh Tien**

## 🤝 Support
Contributions, issues, and feature requests are welcome!
Give a ⭐️ if you like this project!
